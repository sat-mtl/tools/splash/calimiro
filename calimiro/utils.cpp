/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./utils.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>

#include <exif.h>
#include <opencv2/opencv.hpp>

namespace calimiro
{

std::vector<glm::vec3> utils::readVerticesFromPly(AbstractLogger* log, const std::string& filename)
{
    std::vector<glm::vec3> vertices;
    bool read_vertices = false;
    std::ifstream file(filename, std::ios::in);
    if (!file.is_open())
    {
        log->error("readPly:: - \"%\" could not be opened", filename);
        return vertices;
    }

    for (std::string line; std::getline(file, line);)
    {
        std::string::size_type pos;
        if ((pos = line.find("end_header")) == 0)
        {
            read_vertices = true;
        }
        else if (read_vertices)
        {
            pos = 0;
            int index = 0;
            glm::vec3 vertex;
            do
            {
                line = line.substr(pos);
                vertex[index] = std::stof(line);
                index++;
                pos = line.find(" ");
                pos++;
            } while (pos != std::string::npos && index < 3);

            vertices.push_back(vertex);
        }
    }
    log->message("readVerticesFromPly:: - File \"%\" successfully read", filename);
    return vertices;
}

bool utils::writeVerticesToPly(AbstractLogger* log, const std::string& filename, const std::vector<glm::vec3> vertices)
{
    std::ofstream file(filename, std::ios::out);
    if (!file.is_open())
        return false;

    file << "ply" << '\n'
         << "format "
         << "ascii 1.0" << '\n'
         << "element vertex " << vertices.size() << '\n'
         << "property double x" << '\n'
         << "property double y" << '\n'
         << "property double z" << '\n'
         << "end_header" << std::endl;

    for (const auto& vertex : vertices)
    {
        file << vertex.x << ' ' << vertex.y << ' ' << vertex.z << '\n';
    }
    log->message("writeVerticesToPly:: - File \"%\" successfully written", filename);
    return true;
}

double utils::computeMean(const std::vector<double> v)
{
    return std::accumulate(v.begin(), v.end(), 0.0) / v.size();
}

double utils::computeStandardDeviation(const std::vector<double> v, double m)
{
    double accum = 0.0;
    std::for_each(v.begin(), v.end(), [&](const double d) { accum += (d - m) * (d - m); });
    return sqrt(accum / (v.size() - 1));
}

glm::mat3 utils::cholesky3x3(const glm::mat3 A)
{
    double L00 = 0.0, L10 = 0.0, L11 = 0.0, L20 = 0.0, L21 = 0.0, L22 = 0.0;
    L00 = sqrt(A[0][0]);
    L10 = A[1][0] / L00;
    L11 = sqrt(A[1][1] - pow(L10, 2));
    L20 = A[2][0] / L00;
    L21 = (A[2][1] - L20 * L10) / L11;
    L22 = sqrt(A[2][2] - pow(L20, 2) - pow(L21, 2));
    glm::mat3 lower_matrix = {{L00, 0.0, 0.0}, {L10, L11, 0.0}, {L20, L21, L22}};
    return lower_matrix;
}

glm::vec3 utils::backwardSubstitution(const glm::mat3 U, const glm::vec3 B)
{
    glm::vec3 x(0.0, 0.0, 0.0);
    for (size_t i = 2; i < 3; --i)
    {
        x[i] = B[i];
        for (size_t j = i + 1; j < 3; ++j)
        {
            x[i] = x[i] - U[i][j] * x[j];
        }
        x[i] = x[i] / U[i][i];
    }
    return x;
}

glm::vec3 utils::forwardSubstitution(const glm::mat3 L, const glm::vec3 B)
{
    glm::vec3 x(0.0, 0.0, 0.0);
    for (size_t i = 0; i < 3; ++i)
    {
        x[i] = B[i];
        for (size_t j = 0; j < i; ++j)
        {
            x[i] = x[i] - L[i][j] * x[j];
        }
        x[i] = x[i] / L[i][i];
    }
    return x;
}

bool utils::writeMetadata(const std::filesystem::path& filename)
{
    // read file to buffer
    FILE* fp = fopen(filename.c_str(), "rb");

    if (!fp)
    {
        return false;
    }

    // initialize the buffer
    fseek(fp, 0, SEEK_END);
    unsigned long fsize = ftell(fp);
    rewind(fp);
    std::vector<unsigned char> buf(fsize);

    if (fread(&buf[0], 1, fsize, fp) != fsize)
    {
        fclose(fp);
        return false;
    }
    fclose(fp);

    // Parse the metadata
    easyexif::EXIFInfo exif_info;
    if (exif_info.parseFrom(&buf[0], fsize) != PARSE_EXIF_SUCCESS)
    {
        return false;
    }

    cv::FileStorage fs((utils::getWorkspace() / "metadata.yml").string(), cv::FileStorage::WRITE);
    fs << "camera_model" << exif_info.Model.c_str() << "focal_length" << exif_info.FocalLength << "image_width"
       << static_cast<int>(exif_info.ImageWidth) // cv::FileStorage does not support serialization of unsigned int...
       << "image_height" << static_cast<int>(exif_info.ImageHeight);
    fs.release();

    return true;
}
}; // namespace calimiro
