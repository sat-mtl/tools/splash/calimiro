/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./geometry.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdlib.h> // for system()

#include <instant-meshes/bvh.h>
#include <instant-meshes/dedge.h>
#include <instant-meshes/extract.h>
#include <instant-meshes/field.h>
#include <instant-meshes/hierarchy.h>
#include <instant-meshes/meshio.h>
#include <instant-meshes/meshstats.h>
#include <instant-meshes/normal.h>
#include <instant-meshes/subdivide.h>
#include <pmp/SurfaceMesh.h>
#include <pmp/algorithms/SurfaceRemeshing.h>
#include <pmp/algorithms/SurfaceSimplification.h>

#include "./config.h"
#include "./io_obj.h"
#include "./kdtree.h"
#include "./utils.h"
#include "./voxelgrid.h"

int nprocs = -1; // Temporary declared and initialized here
namespace calimiro
{

Geometry::Geometry(AbstractLogger* log,
    const std::vector<glm::vec3>& vertices,
    const std::vector<glm::vec3>& normals,
    const std::vector<glm::vec2>& uvs,
    const std::vector<std::vector<FaceVertex>>& faces)
    : _log(log)
    , _vertices(vertices)
    , _normals(normals)
    , _uvs(uvs)
    , _faces(faces)
    , _nb_vertices(vertices.size())
{
}

Geometry Geometry::computeNormalsPointSet(unsigned int nb_neighbors)
{
    std::filesystem::path workspace = std::filesystem::path(utils::getWorkspace());
    // if the directory exists already, the function does nothing
    std::filesystem::create_directory(workspace);

    std::string input = workspace / std::string("tmp_in.obj");
    std::string output = workspace / std::string("tmp_out.obj");
    std::string script_location = workspace / std::string("computePointNormals.mlx");

    // The geometry has to be provided as an obj file to MeshlabServer -> the geometry is saved beforehand
    Obj obj(_log, *this);
    obj.writeMesh(input);
    // Same for the filter to apply that has to be provided as an mlx file...
    if (!Geometry::generateComputeNormalsScript(script_location, nb_neighbors))
    {
        _log->error("Geometry::generateComputeNormalsScript - Could not produce the file");
        return Geometry(_log);
    }

    std::string command = std::string(MESHLABSERVER) + " -i " + input + " -o " + output + " -s " + script_location;
    if (std::system(command.c_str()) != 0)
    {
        _log->error("Geometry::generateComputeNormalsScript - Could not execute \"%\"", command);
    }

    Obj obj_tmp = Obj(_log);
    obj_tmp.readMesh(output);
    Geometry geometry = obj_tmp;

    return geometry;
}

Geometry Geometry::denoise(double threshold, int nb_neighbors)
{
    // Create the kdtree
    Kdtree kd;

    // For each points, get its closest neighbor distances
    std::vector<std::vector<double>> neighbor_distances;

    for (const auto& vertex : _vertices)
    {
        kd.insert(vertex);
        neighbor_distances.push_back(kd.nearest_neighbors_priorities(vertex, nb_neighbors));
    }

    std::vector<double> mean_of_neighbors;
    std::vector<int> rejected_index;
    double m;
    for (const auto& distances : neighbor_distances)
    {
        m = utils::computeMean(distances); // the average distance of a point's k-nearest neighbors
        mean_of_neighbors.push_back(m);
    }

    m = utils::computeMean(mean_of_neighbors); // mean of the mean... (the mean of the average distance of a point's k-nearest neighbors)
    double stdev;

    // for each point, if the stdev of its neighbor distances is > than a threshold, the point is an outlier
    for (std::size_t i = 0; i < _nb_vertices; ++i)
    {
        stdev = utils::computeStandardDeviation(neighbor_distances[i], m);
        if (stdev > threshold)
            rejected_index.push_back(i);
    }

    if (rejected_index.empty())
    {
        return Geometry(_log, _vertices, _normals, _uvs, {});
    }
    else
    {
        // Remove the points and update the geometry
        std::vector<glm::vec3> new_pts;
        std::vector<glm::vec3> new_normals;
        std::vector<glm::vec2> new_uvs;
        bool update_normals = _normals.size() == _nb_vertices ? 1 : 0;
        bool update_uvs = _uvs.size() == _nb_vertices ? 1 : 0;
        for (std::size_t i = 0; i < _nb_vertices; ++i)
        {
            // verify that the point is not in the outlier list
            if (!std::binary_search(rejected_index.begin(), rejected_index.end(), i))
            {
                new_pts.push_back(_vertices[i]);
                if (update_normals)
                    new_normals.push_back(_normals[i]);
                if (update_uvs)
                    new_uvs.push_back(_uvs[i]);
            }
        }
        return Geometry(_log, new_pts, new_normals, new_uvs, {});
    }
}

Geometry Geometry::downsample(double grid_step)
{
    // Create the voxel grid
    Voxelgrid voxelgrid(_vertices, grid_step);
    std::vector<Voxelgrid::Linspace> grd = voxelgrid.grid();
    std::vector<size_t> nb_vxl_xyz = voxelgrid.number_voxel_xyz();

    int index;
    std::vector<int> tmp_row;
    std::vector<std::vector<int>> voxel_ids;

    // associate each point to a voxel in the grid
    for (const auto& pt : _vertices)
    {
        tmp_row.clear();
        for (size_t j = 0; j < 3; ++j)
        {
            auto it = upper_bound(grd[j].linspaced.begin(), grd[j].linspaced.end(), pt[j]);
            index = it - grd[j].linspaced.begin() - 1;
            tmp_row.push_back(index);
        }
        voxel_ids.push_back(tmp_row);
    }

    // for each voxel, get the points enclosed in the voxel
    std::vector<std::reference_wrapper<const glm::vec3>> voxel_references;
    std::vector<std::vector<std::reference_wrapper<const glm::vec3>>> arr(nb_vxl_xyz[0] * nb_vxl_xyz[1] * nb_vxl_xyz[2]);
    int index_3dto1d;
    for (std::size_t i = 0; i < voxel_ids.size(); ++i)
    {
        auto id = voxel_ids[i];
        index_3dto1d = id[0] + nb_vxl_xyz[0] * (id[1] + (nb_vxl_xyz[1] * id[2]));
        arr[index_3dto1d].push_back(std::cref(_vertices[i]));
    }

    // for each voxel, compute the centroid of the points
    glm::vec3 centroid;
    std::vector<glm::vec3> centroid_pts;
    for (auto& voxel_references : arr)
    {
        // Make sure that the voxel is not empty
        if (!voxel_references.empty())
        {
            centroid = utils::computeMeanOfVectors(voxel_references);
            centroid_pts.push_back(centroid);
        }
    }
    return Geometry(_log, centroid_pts, {}, {}, {});
}

Geometry::SphereShape Geometry::leastSquareSphereFit()
{
    double x_center = 0, y_center = 0, z_center = 0;
    double a00 = 0, a11 = 0, a22 = 0, a01 = 0, a02 = 0, a12 = 0, b0 = 0, b1 = 0, b2 = 0;

    for (const auto& coords : _vertices)
    {
        x_center += coords.x;
        y_center += coords.y;
        z_center += coords.z;
    }

    x_center = x_center / _nb_vertices;
    y_center = y_center / _nb_vertices;
    z_center = z_center / _nb_vertices;

    for (const auto& coords : _vertices)
    {
        a00 += (coords.x * (coords.x - x_center));
        a11 += (coords.y * (coords.y - y_center));
        a22 += (coords.z * (coords.z - z_center));
        a01 += (coords.x * (coords.y - y_center));
        a02 += (coords.x * (coords.z - z_center));
        a12 += (coords.y * (coords.z - z_center));
        b0 += (coords.x - x_center) * (pow(coords.x, 2) + pow(coords.y, 2) + pow(coords.z, 2));
        b1 += (coords.y - y_center) * (pow(coords.x, 2) + pow(coords.y, 2) + pow(coords.z, 2));
        b2 += (coords.z - z_center) * (pow(coords.x, 2) + pow(coords.y, 2) + pow(coords.z, 2));
    }

    // Notice A is a symmetric matrix
    double two_over_nb_pts = 2.0 / _nb_vertices;
    glm::mat3 A = {{a00 * two_over_nb_pts, a01 * two_over_nb_pts, a02 * two_over_nb_pts},
        {a01 * two_over_nb_pts, a11 * two_over_nb_pts, a12 * two_over_nb_pts},
        {a02 * two_over_nb_pts, a12 * two_over_nb_pts, a22 * two_over_nb_pts}};
    glm::vec3 B = {b0 / _nb_vertices, b1 / _nb_vertices, b2 / _nb_vertices};
    // We want to solve for x in Ax = B
    // A is a positive-definite matrix -> cholesky decomposition can be used: A = L*L_transpose
    glm::mat3 L;
    L = utils::cholesky3x3(A);
    glm::mat3 L_transpose = {{L[0][0], L[1][0], L[2][0]}, {L[0][1], L[1][1], L[2][1]}, {L[0][2], L[1][2], L[2][2]}};
    Geometry::SphereShape sphereShape;
    sphereShape.center = utils::backwardSubstitution(L_transpose, utils::forwardSubstitution(L, B));
    _log->message("Geometry::leastSquareSphereFit\n"
                  "\tCenter coordinates: %, %, %",
        std::to_string(sphereShape.center.x),
        std::to_string(sphereShape.center.y),
        std::to_string(sphereShape.center.z));

    // compute the radius
    sphereShape.radius = 0;

    // the sphere is parametrized by (x - x_c)**2 + (y - y_c)**2 + (z - z_c)**2 = R**2
    // the mean R**2 is first computed, then the squared root is taken
    for (const auto& coords : _vertices)
        sphereShape.radius = sphereShape.radius + pow(coords.x - sphereShape.center.x, 2) + pow(coords.y - sphereShape.center.y, 2) + pow(coords.z - sphereShape.center.z, 2);

    sphereShape.radius = sqrt(sphereShape.radius / _nb_vertices);
    _log->message("Geometry::leastSquareSphereFit:\n\tRadius: %", std::to_string(sphereShape.radius));
    return sphereShape;
}

Geometry Geometry::marchingCubes(unsigned int resolution)
{
    std::filesystem::path workspace = std::filesystem::path(utils::getWorkspace());
    // if the directory exists already, the function does nothing
    std::filesystem::create_directory(workspace);

    std::string input = workspace / std::string("tmp_in.obj");
    std::string output = workspace / std::string("tmp_out.obj");
    std::string script_location = workspace / std::string("marchingcubes.mlx");

    // The geometry has to be provided as an obj file to MeshlabServer -> the geometry is saved beforehand
    Obj obj(_log, *this);
    obj.writeMesh(input);

    // Same for the filter to apply that has to be provided as an mlx file...
    if (!Geometry::generateMarchingCubesScript(script_location, resolution))
    {
        _log->error("Geometry::generateMarchingCubesScript - Could not produce the file");
        return Geometry(_log);
    }

    std::string command = std::string(MESHLABSERVER) + " -i " + input + " -o " + output + " -s " + script_location;

    if (std::system(command.c_str()) != 0)
    {
        _log->error("Geometry::generateMarchingCubesScript - Could not execute \"%\"", command);
    }

    Obj obj_tmp = Obj(_log);
    obj_tmp.readMesh(output);
    Geometry geometry = obj_tmp;

    return geometry;
}

Geometry Geometry::normalizeSphere(const SphereShape& sphereShape)
{
    std::vector<glm::vec3> normalized_vertices;
    normalized_vertices.reserve(_nb_vertices);
    for (const auto& coords : _vertices)
    {
        normalized_vertices.emplace_back(glm::vec3{(coords.x - sphereShape.center.x) / sphereShape.radius,
            (coords.y - sphereShape.center.y) / sphereShape.radius,
            (coords.z - sphereShape.center.z) / sphereShape.radius});
    }
    return Geometry(_log, normalized_vertices, _normals, _uvs, _faces);
}

Geometry Geometry::computeTextureCoordinates(TexCoordGen* generator)
{
    // clear the previous _uvs
    _uvs.clear();

    // compute the texture coordinates using the appropriate method
    auto new_uvs = generator->computeTextureCoordinates();

    // Update the faces uvID
    for (auto& f : _faces)
    {
        // the uvs are computed from the corresponding vertex -> uvID = vertexID
        for (auto& face_vertex : f)
            face_vertex.uvID = face_vertex.vertexID;
    }

    return Geometry(_log, _vertices, _normals, new_uvs, _faces);
}

Geometry Geometry::simplifyGeometry(int rosy, int posy, int face_count, int vertex_count, float scale)
{
    std::filesystem::path workspace = std::filesystem::path(utils::getWorkspace());
    // if the directory exists already, the function does nothing
    std::filesystem::create_directory(workspace);

    std::string input = workspace / std::string("tmp_in_instant-meshes.obj");
    std::string output = workspace / std::string("tmp_out_instant-meshes.obj");

    // The geometry has to be provided as an obj file to Instant-Meshes -> the geometry is saved beforehand
    Obj obj(_log, *this);
    obj.writeMesh(input);

    // To have a triangular mesh -> rosy = 6 and posy = 3
    bool extrinsic = true, align_to_boundaries = false;
    bool deterministic = false, pure_quad = true;
    uint32_t smooth_iter = 2;
    Float creaseAngle = -1;
    scale = static_cast<Float>(scale);

    // Verification of the position field and orientation field
    if ((posy != 3 && posy != 4) || (rosy != 2 && rosy != 4 && rosy != 6))
    {
        _log->error("Geometry::simplifyGeometry - Error: Invalid symmetry type!");
        return Geometry(_log);
    }

    std::set<uint32_t> crease_in, crease_out;
    std::unique_ptr<BVH> bvh;
    AdjacencyMatrix adj = nullptr;
    Timer<> timer;

    MatrixXu F;
    MatrixXf V, N;
    VectorXf A;
    load_mesh_or_pointcloud(input, F, V, N);
    MeshStats stats = compute_mesh_stats(F, V, deterministic);
    if (scale < 0 && vertex_count < 0 && face_count < 0)
    {
        _log->warning("Geometry::simplifyGeometry - No target vertex count/face count/scaleargument provided. Setting to the default of 1/16 * input vertex count");
        vertex_count = V.cols() / 16;
    }
    if (scale > 0)
    {
        Float face_area = posy == 4 ? (scale * scale) : (std::sqrt(3.f) / 4.f * scale * scale);
        face_count = stats.mSurfaceArea / face_area;
        vertex_count = posy == 4 ? face_count : (face_count / 2);
    }
    else if (face_count > 0)
    {
        Float face_area = stats.mSurfaceArea / face_count;
        vertex_count = posy == 4 ? face_count : (face_count / 2);
        scale = posy == 4 ? std::sqrt(face_area) : (2 * std::sqrt(face_area * std::sqrt(1.f / 3.f)));
    }
    else if (vertex_count > 0)
    {
        face_count = posy == 4 ? vertex_count : (vertex_count * 2);
        Float face_area = stats.mSurfaceArea / face_count;
        scale = posy == 4 ? std::sqrt(face_area) : (2 * std::sqrt(face_area * std::sqrt(1.f / 3.f)));
    }

    _log->message("Geometry::simplifyGeometry - Output mesh goals (approximate"
                  "\nVertex count %"
                  "\nFace count  %"
                  "\nEdge length %",
        std::to_string(vertex_count),
        std::to_string(face_count),
        std::to_string(scale));

    MultiResolutionHierarchy mRes;
    // Subdivide the mesh if necessary
    VectorXu V2E, E2E;
    VectorXb boundary, nonManifold;
    if (stats.mMaximumEdgeLength * 2 > scale || stats.mMaximumEdgeLength > stats.mAverageEdgeLength * 2)
    {
        _log->warning("Geometry::simplifyGeometry - Input mesh is too coarse for the desired output edge length"
                      "(max input mesh edge length= %), subdividing ..",
            std::to_string(stats.mMaximumEdgeLength));
        build_dedge(F, V, V2E, E2E, boundary, nonManifold);
        subdivide(F, V, V2E, E2E, boundary, nonManifold, std::min(scale / 2, (Float)stats.mAverageEdgeLength * 2), deterministic);
    }

    // Compute a directed edge data structure
    build_dedge(F, V, V2E, E2E, boundary, nonManifold);

    // Compute adjacency matrix
    adj = generate_adjacency_matrix_uniform(F, V2E, E2E, nonManifold);

    // Compute vertex/crease normals
    if (creaseAngle >= 0)
        generate_crease_normals(F, V, V2E, E2E, boundary, nonManifold, creaseAngle, N, crease_in);
    else
        generate_smooth_normals(F, V, V2E, E2E, nonManifold, N);

    // Compute dual vertex areas
    compute_dual_vertex_areas(F, V, V2E, E2E, nonManifold, A);
    mRes.setE2E(std::move(E2E));
    // Build multi-resolution hierarchy
    mRes.setAdj(std::move(adj));
    mRes.setF(std::move(F));
    mRes.setV(std::move(V));
    mRes.setA(std::move(A));
    mRes.setN(std::move(N));
    mRes.setScale(scale);
    mRes.build(deterministic);
    mRes.resetSolution();
    if (align_to_boundaries)
    {
        mRes.clearConstraints();
        for (uint32_t i = 0; i < 3 * mRes.F().cols(); ++i)
        {
            if (mRes.E2E()[i] == INVALID)
            {
                uint32_t i0 = mRes.F()(i % 3, i / 3);
                uint32_t i1 = mRes.F()((i + 1) % 3, i / 3);
                Vector3f p0 = mRes.V().col(i0), p1 = mRes.V().col(i1);
                Vector3f edge = p1 - p0;
                if (edge.squaredNorm() > 0)
                {
                    edge.normalize();
                    mRes.CO().col(i0) = p0;
                    mRes.CO().col(i1) = p1;
                    mRes.CQ().col(i0) = mRes.CQ().col(i1) = edge;
                    mRes.CQw()[i0] = mRes.CQw()[i1] = mRes.COw()[i0] = mRes.COw()[i1] = 1.0f;
                }
            }
        }
        mRes.propagateConstraints(rosy, posy);
    }

    if (smooth_iter > 0)
    {
        bvh = std::make_unique<BVH>(&mRes.F(), &mRes.V(), &mRes.N(), stats.mAABB);
        bvh->build();
    }

    _log->message("Geometry::simplifyGeometry - Preprocessing is done. (total time excluding file I/O: %)", timeString(timer.reset()));

    Optimizer optimizer(mRes, false);
    optimizer.setRoSy(rosy);
    optimizer.setPoSy(posy);
    optimizer.setExtrinsic(extrinsic);

    _log->message("Geometry::simplifyGeometry - Optimizing orientation field ..");
    optimizer.optimizeOrientations(-1);
    optimizer.notify();
    optimizer.wait();
    _log->message("done. (took %)", timeString(timer.reset()));

    std::map<uint32_t, uint32_t> sing;
    compute_orientation_singularities(mRes, sing, extrinsic, rosy);
    _log->message("Geometry::simplifyGeometry - Orientation field has %  singularities", std::to_string(sing.size()));
    timer.reset();

    _log->message("Optimizing position field .. ");
    optimizer.optimizePositions(-1);
    optimizer.notify();
    optimizer.wait();
    _log->message("done. (took %)", timeString(timer.reset()));

    optimizer.shutdown();

    MatrixXf O_extr, N_extr, Nf_extr;
    std::vector<std::vector<TaggedLink>> adj_extr;
    extract_graph(mRes, extrinsic, rosy, posy, adj_extr, O_extr, N_extr, crease_in, crease_out, deterministic);

    MatrixXu F_extr;
    extract_faces(adj_extr, O_extr, N_extr, Nf_extr, F_extr, posy, mRes.scale(), crease_out, true, pure_quad, bvh.get(), smooth_iter);
    _log->message("Geometry::simplifyGeometry - Extraction is done. (total time: %)", timeString(timer.reset()));

    write_mesh(output, F_extr, O_extr, MatrixXf(), Nf_extr);

    Obj obj_tmp = Obj(_log);
    obj_tmp.readMesh(output);
    Geometry geometry = obj_tmp;

    return geometry;
}

Geometry Geometry::simplifyGeometryPMP(int target_percentage)
{
    std::filesystem::path workspace = std::filesystem::path(utils::getWorkspace());
    // if the directory exists already, the function does nothing
    std::filesystem::create_directory(workspace);

    std::string input = workspace / std::string("tmp_in_pmp-library.obj");
    std::string output = workspace / std::string("tmp_out_pmp-library.obj");

    Obj obj(_log, *this);
    obj.writeMesh(input);
    pmp::SurfaceMesh mesh;
    mesh.read(input);

    // Mesh decimation
    pmp::SurfaceSimplification ss(mesh);
    int aspect_ratio = 10;
    int normal_deviation = 180;
    ss.initialize(aspect_ratio, 0.0, 0.0, normal_deviation, 0.0);
    ss.simplify(mesh.n_vertices() * 0.01 * target_percentage);

    // Perform uniform remeshing
    pmp::Scalar edge_length(0);
    for (const auto eit : mesh.edges())
    {
        edge_length += pmp::distance(mesh.position(mesh.vertex(eit, 0)), mesh.position(mesh.vertex(eit, 1)));
    }

    edge_length /= static_cast<pmp::Scalar>(mesh.n_edges());
    pmp::SurfaceRemeshing(mesh).uniform_remeshing(edge_length);

    mesh.write(output);

    Obj obj_tmp = Obj(_log);
    obj_tmp.readMesh(output);
    Geometry geometry = obj_tmp;

    return geometry;
}

bool Geometry::generateComputeNormalsScript(const std::string& filename, unsigned int nb_neighbors)
{
    std::ofstream file(filename, std::ios::out);
    if (!file.is_open())
    {
        return false;
    }

    file << "<!DOCTYPE FilterScript>" << '\n'
         << "<FilterScript>" << '\n'
         << "<filter name=\"Compute normals for point sets\">" << '\n'
         << "<Param tooltip=\"The number of neighbors used to estimate normals.\""
            " description=\"Neighbour num\" name=\"K\" value=\"" +
                std::to_string(nb_neighbors) + "\" type=\"RichInt\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"The number of smoothing iteration done on the p used to"
            " estimate and propagate normals.\" description=\"Smooth Iteration\" "
            "name=\"smoothIter\" value=\"0\" type=\"RichInt\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"If the 'viewpoint' (i.e. scanner position) is known, it can "
            "be used to disambiguate normals orientation, so that all the normals will be "
            "oriented in the same direction.\" description=\"Flip normals w.r.t. viewpoint\" "
            "name=\"flipFlag\" value=\"false\" type=\"RichBool\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"The viewpoint position can be set by hand (i.e. getting the "
            "current viewpoint) or it can be retrieved from mesh camera, if the viewpoint "
            "position is stored there.\" x=\"0\" y=\"0\" description=\"Viewpoint Pos.\" "
            "z=\"0\" name=\"viewPos\" type=\"RichPoint3f\" isxmlparam=\"0\"/>"
         << '\n'
         << "</filter>" << '\n'
         << "</FilterScript>";

    return true;
}

bool Geometry::generateMarchingCubesScript(const std::string& filename, unsigned int resolution)
{
    std::ofstream file(filename, std::ios::out);
    if (!file.is_open())
    {
        return false;
    }

    file << "<!DOCTYPE FilterScript>" << '\n'
         << "<FilterScript>" << '\n'
         << "<filter name=\"Compute normals for point sets\">" << '\n'
         << "<Param tooltip=\"The number of neighbors used to estimate normals.\""
            " description=\"Neighbour num\" name=\"K\" value=\"10\" type=\"RichInt\" "
            "isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"The number of smoothing iteration done on the p used to"
            " estimate and propagate normals.\" description=\"Smooth Iteration\" "
            "name=\"smoothIter\" value=\"0\" type=\"RichInt\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"If the 'viewpoint' (i.e. scanner position) is known, it can "
            "be used to disambiguate normals orientation, so that all the normals will be "
            "oriented in the same direction.\" description=\"Flip normals w.r.t. viewpoint\" "
            "name=\"flipFlag\" value=\"false\" type=\"RichBool\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"The viewpoint position can be set by hand (i.e. getting the "
            "current viewpoint) or it can be retrieved from mesh camera, if the viewpoint "
            "position is stored there.\" x=\"0\" y=\"0\" description=\"Viewpoint Pos.\" "
            "z=\"0\" name=\"viewPos\" type=\"RichPoint3f\" isxmlparam=\"0\"/>"
         << '\n'
         << "</filter>" << '\n'
         << "<filter name=\"Marching Cubes (APSS)\">" << '\n'
         << "<Param tooltip=\"Scale of the spatial low pass filter.&#xa;It is relative to "
            "the radius (local point spacing) of the vertices.\" description=\"MLS - Filter "
            "scale\" name=\"FilterScale\" value=\"2\" type=\"RichFloat\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"Threshold value used to stop the projections.&#xa;This value "
            "is scaled by the mean point spacing to get the actual threshold.\" description="
            "\"Projection - Accuracy (adv)\" name=\"ProjectionAccuracy\" value=\"0.0001\" type="
            "\"RichFloat\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"Max number of iterations for the projection.\" description=\""
            "Projection - Max iterations (adv)\" name=\"MaxProjectionIters\" value=\"15\" "
            "type=\"RichInt\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"Control the curvature of the fitted spheres: 0 is equivalent "
            "to a pure plane fit,1 to a pure spherical fit, values between 0 and 1 gives "
            "intermediate results,while other real values might give interesting results, "
            "but take care with extremesettings !\" description=\"MLS - Spherical parameter\" "
            "name=\"SphericalParameter\" value=\"1\" type=\"RichFloat\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"If checked, use the accurate MLS gradient instead of the local "
            "approximationto compute the normals.\" description=\"Accurate normals\" "
            "name=\"AccurateNormal\" value=\"true\" type=\"RichBool\" isxmlparam=\"0\"/>"
         << '\n'
         << "<Param tooltip=\"The resolution of the grid on which we run the marching cubes."
            "This marching cube is memory friendly, so you can safely set large values up to "
         << "1000 or even more.\" description=\"Grid Resolution\" name=\"Resolution\" value=\"" +
            std::to_string(resolution) + "\" type=\"RichInt\" isxmlparam=\"0\"/>" << '\n'
         << "</filter>" << '\n'
         << "</FilterScript>";

    return true;
}

} // namespace calimiro
