/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_BQUEUE__
#define __CALIMIRO_BQUEUE__

#include <limits>
#include <vector>

namespace calimiro
{

template <typename T>
class Bqueue
{
  public:
    struct Elt
    {
        Elt() = default;
        Elt(const T* v, double p)
            : value(v)
            , priority(p)
        {
        }
        const T* value = nullptr;
        double priority = std::numeric_limits<double>::infinity();
    };

    // Construct a bounded priority queue
    Bqueue(const size_t);

    // Returns the number of elements in the bounded priority queue
    std::size_t size() const { return _sz; };

    // Returns the maximum number of elements the queue can contains
    std::size_t capacity() const { return _k; };

    // Returns true if the size of the queue is 0
    bool empty() const;

    // Returns the heap
    std::vector<Elt> queue() const { return _heap; };

    // Returns the prorities contained in the heap
    std::vector<double> priorities() const;

    Elt peek();
    Elt pop();
    void insert(const T& value, double priority);

  private:
    size_t _sz{0}; // Current number of elt stored in the bounded priority queue
    std::vector<Elt> _heap;
    size_t _k; // Maximum capacity of the queue
    size_t left_child_index(size_t) const;
    size_t right_child_index(size_t) const;
    size_t parent_index(size_t) const;
    void heapify_up(const size_t);
    void heapify_down(const size_t);
};

}; // namespace calimiro

// ************ IMPLEMENTATION OF THE BQUEUE CLASS ************ //

namespace calimiro
{
template <typename T>
Bqueue<T>::Bqueue(const size_t capacity)
    : _k(capacity)
{
    _heap.reserve(_k);
}

template <typename T>
bool Bqueue<T>::empty() const
{
    if (_sz == 0)
        return true;
    else
        return false;
}

template <typename T>
size_t Bqueue<T>::left_child_index(size_t index) const
{
    return 2 * index + 1;
}

template <typename T>
size_t Bqueue<T>::right_child_index(size_t index) const
{
    return 2 * index + 2;
}

template <typename T>
size_t Bqueue<T>::parent_index(size_t index) const
{
    return index / 2;
}

// Mostly used by insert, when an element is added at the end of the heap's
// internal array to make sure the heap property is maintained
template <typename T>
void Bqueue<T>::heapify_up(const size_t index)
{
    size_t curr_index = index;
    while (curr_index != 0 && _heap[parent_index(curr_index)].priority < _heap[curr_index].priority)
    {
        std::swap(_heap[curr_index], _heap[parent_index(curr_index)]);
        curr_index = parent_index(curr_index);
    }
}

// Typically used to push down an element after getting the max of the heap
template <typename T>
void Bqueue<T>::heapify_down(const size_t index)
{
    size_t curr_index = index;
    size_t largest_child_index;

    while (left_child_index(curr_index) < _sz)
    {
        // Get the largest child between left and right
        largest_child_index = left_child_index(curr_index);
        if (right_child_index(curr_index) < _sz && _heap[right_child_index(curr_index)].priority > _heap[largest_child_index].priority)
            largest_child_index = right_child_index(curr_index);

        // Compare the value of the element to the child, make sure the heap property is maintained
        if (_heap[curr_index].priority > _heap[largest_child_index].priority)
            break;
        else
        {
            std::swap(_heap[curr_index], _heap[largest_child_index]);
            curr_index = largest_child_index;
        }
    }
}

// Display/show the maximum element of the queue
template <typename T>
typename Bqueue<T>::Elt Bqueue<T>::peek()
{
    if (empty())
        return Elt();
    else
        return _heap[0];
}

// Extract the maximum element of the queue-> the root of the heap
template <typename T>
typename Bqueue<T>::Elt Bqueue<T>::pop()
{
    if (empty())
        return Elt();
    else
    {
        Elt max_elt = _heap[0];
        Elt last_elt = _heap[_sz - 1];
        _heap.pop_back();
        --_sz;
        if (_heap.size() > 0)
        {
            _heap[0] = last_elt;
            heapify_down(0); // reorder the elements of the heap now that there's a new root
        }
        return max_elt;
    }
}

// Insert element only if priority(new_elt) < max(elt of queue) for k == max capacity
// if k < max size -> insert the element
template <typename T>
void Bqueue<T>::insert(const T& value, double priority)
{
    Elt elt(&value, priority);
    // if k < max size -> insert the element
    if (_sz < _k)
    {
        _heap.push_back(elt);
        ++_sz;
        heapify_up(_sz - 1);
    }
    else
    {
        // the queue is full. we need to verify that the new elt has a value that is lower than the max in the array
        if (elt.priority < peek().priority)
        {
            pop();
            _heap.push_back(elt);
            ++_sz;
            heapify_up(_sz - 1);
        }
    }
}

template <typename T>
std::vector<double> Bqueue<T>::priorities() const
{
    std::vector<double> priorities;
    for (const auto& elt : _heap)
    {
        priorities.push_back(elt.priority);
    }
    return priorities;
}

}; // namespace calimiro

#endif // __CALIMIRO_BQUEUE__
