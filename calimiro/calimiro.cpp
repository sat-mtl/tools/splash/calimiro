#include <iostream>

#include "./calimiro.h"

int main()
{
    calimiro::Logger logger;
    calimiro::Reconstruction recon = calimiro::Reconstruction(&logger);
    recon.sfmInitImageListing(7200);
    recon.computeFeatures();
    recon.computeMatches();
    recon.incrementalSfM();
    recon.computeStructureFromKnownPoses();
    recon.convertSfMStructure();
    return 0;
}
