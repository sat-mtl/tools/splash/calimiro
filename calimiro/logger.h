/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

// This header is a copy of shmdata console-logger.hpp.
// The library can be found here: https://gitlab.com/sat-metalab/shmdata/tree/master

#ifndef __CALIMIRO_LOGGER__
#define __CALIMIRO_LOGGER__

#include <iostream>

#include "./abstract_logger.h"

namespace calimiro
{

class Logger : public AbstractLogger
{
  public:
    void set_debug(bool debug) { debug_ = debug; }

  private:
    bool debug_{true};
    void on_error(std::string&& str) final { std::cerr << "\033[1;31merror: " << str << "\033[0m" << std::endl; }
    void on_critical(std::string&& str) final { std::cerr << "\033[1;31mcritical: " << str << "\033[0m" << std::endl; }
    void on_warning(std::string&& str) final { std::cerr << "\033[1;33mwarning: " << str << "\033[0m" << std::endl; }
    void on_message(std::string&& str) final { std::cout << "message: " << str << std::endl; }
    void on_info(std::string&& str) final { std::cout << "info: " << str << std::endl; }
    void on_debug(std::string&& str) final
    {
        if (debug_)
            std::cout << "\033[0;33mdebug: " << str << "\033[0m" << std::endl;
    }
};

} // namespace calimiro

#endif // __CALIMIRO_LOGGER__
