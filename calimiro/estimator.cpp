/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./estimator.h"

#include <iostream>
#include <limits> // for numeric_limits

#define GLM_ENABLE_EXPERIMENTAL
#include <RansacLib/ransac.h>
#include <glm/gtx/euler_angles.hpp>

#include "./optimizer.h"
#include "./utils.h"

namespace calimiro
{
size_t Kernel::MinimalSolver(const std::vector<int>& samples, ModelVector* models) const
{
    if (samples.size() < MINIMUM_SAMPLES)
    {
        _log->error("Kernel::Fit - Insufficient number of calibration points");
        return 0;
    }

    // Extract the sampled points
    std::vector<CalibrationPoint> sampled_xs = utils::extractElts(_xs, samples);
    std::srand(std::time(nullptr)); // initialize a random seed
    if (!_camera_ptr)
    {
        _log->error("Kernel::Fit - Unable to estimate the parameters, cannot access the camera object");
        return 0;
    }

    size_t nb_params = _camera_ptr->getParams().size() + _nb_extrinsic_params;
    std::vector<double> selectedValues = _optimizer->optimize(nb_params, sampled_xs);

    models->push_back(selectedValues);
    return models->size();
}

double Kernel::EvaluateModelOnPoint(const Model& parameters, int sample_id) const
{
    // Compute the new intrinsics and extrinsics parameters
    std::vector<double> intrinsics;
    for (size_t i = 0; i < parameters.size() - _nb_extrinsic_params; ++i)
    {
        intrinsics.push_back(parameters[i]);
    }
    glm::dvec3 eye;
    glm::dvec3 euler;
    for (int i = 0; i < 3; ++i)
    {
        eye[i] = parameters[parameters.size() - 6 + i];
        euler[i] = parameters[parameters.size() - 3 + i];
    }

    _camera_ptr->updateFromParams(intrinsics);
    _camera_ptr->updateExtrinsics(eye, euler);
    Camera::Transforms transforms(Camera::computeTransformationMatrices(_camera_ptr->extrinsics(), _camera_ptr->intrinsics()));
    glm::dvec4 cam_coordinates = transforms.lookM * glm::dvec4(_xs[sample_id].world, 1.0);
    glm::dvec4 clip_coordinates = transforms.projM * cam_coordinates;
    glm::dvec3 ndc_coordinates = glm::dvec3{clip_coordinates.x, clip_coordinates.y, clip_coordinates.z} / clip_coordinates.w;
    glm::dvec2 screen_coordinates = glm::dvec2{transforms.viewportV[2] / 2.0 * ndc_coordinates.x + transforms.viewportV[0] + transforms.viewportV[2] / 2.0,
        transforms.viewportV[3] / 2.0 * ndc_coordinates.y + transforms.viewportV[1] + transforms.viewportV[3] / 2.0};
    glm::dvec2 distorted_pixel = _camera_ptr->getDistortedPixel(screen_coordinates);
    return _optimizer->error(_xs[sample_id], distorted_pixel, cam_coordinates.z);
};

Kernel::Model Kernel::Ransac(std::vector<int>& /*vec_inliers*/, float threshold, int seed)
{
    ransac_lib::LORansacOptions options;
    options.min_num_iterations_ = 100u;
    options.max_num_iterations_ = 1000u;
    options.squared_inlier_threshold_ = threshold;

    options.random_seed_ = seed;

    ransac_lib::LocallyOptimizedMSAC<Kernel::Model, Kernel::ModelVector, Kernel> lomsac;
    ransac_lib::RansacStatistics ransac_stats;
    Kernel::Model best_model;
    int num_ransac_inliers = lomsac.EstimateModel(options, *this, &best_model, &ransac_stats);

    _log->message("Kernel::Ransac: LOMSAC found % inliers in % iterations with an inlier ratio of %",
        std::to_string(num_ransac_inliers),
        std::to_string(ransac_stats.num_iterations),
        std::to_string(ransac_stats.inlier_ratio));

    return best_model;
};

} // namespace calimiro
