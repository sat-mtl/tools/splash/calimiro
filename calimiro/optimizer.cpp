/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./optimizer.h"

#include <iostream>
#include <math.h>
#include <time.h> // for the seed of rand

#include <gsl/gsl_blas.h>
#include <gsl/gsl_multimin.h>

#include "./camera_model.h"
#include "./estimator.h"

namespace calimiro
{

// In the camera/eye world, the camera is at the origin looking along the negative Z-axis
// we apply a penalty to solutions with a positive value for the z of the camera world (Z > 0) -> the points are behind the camera
double Optimizer::penalty(const double z_value)
{
    return 100000.0 * (z_value < 0.0 ? 0.0 : 1.0);
}

double Levenberg_Marquardt::error(const CalibrationPoint& pt, const glm::dvec2& pixel)
{
    double distance = pt.weight * pow(pt.screen.x - pixel.x, 2.0) + pow(pt.screen.y - pixel.y, 2.0);
    return sqrt(distance);
}

int Levenberg_Marquardt::costFunct(const gsl_vector* x, void* params, gsl_vector* f)
{
    if (params == nullptr)
        return GSL_EINVAL;

    auto container = reinterpret_cast<Kernel::Container*>(params);
    double fov = gsl_vector_get(x, 0);
    double cx = gsl_vector_get(x, 1);
    double cy = gsl_vector_get(x, 2);

    // Some limits for the calibration parameters
    if (fov < 4.0 || fov > 120.0 || abs(cx - 0.5) > 1.0 || abs(cy - 0.5) > 1.0)
        return GSL_ERANGE;

    glm::dvec3 eye;
    glm::dvec3 euler;
    for (int i = 0; i < 3; ++i)
    {
        eye[i] = gsl_vector_get(x, x->size - 6 + i);
        euler[i] = gsl_vector_get(x, x->size - 3 + i);
    }

    std::vector<double> intrinsics;
    for (size_t i = 0; i < x->size - 6; ++i) // minus 6, we don't want the eye and euler angles
    {
        intrinsics.push_back(gsl_vector_get(x, i));
    }

    container->cam_ptr->updateFromParams(intrinsics);
    container->cam_ptr->updateExtrinsics(eye, euler);
    Camera::Transforms transforms(Camera::computeTransformationMatrices(container->cam_ptr->extrinsics(), container->cam_ptr->intrinsics()));

    // Project all the object points, and measure the distance between them and the image points
    size_t i = 0;
    for (const auto& pt : container->pts)
    {
        glm::dvec4 cam_coordinates = transforms.lookM * glm::dvec4(pt.world, 1.0);
        glm::dvec4 clip_coordinates = transforms.projM * cam_coordinates;
        glm::dvec3 ndc_coordinates = glm::dvec3(clip_coordinates.x, clip_coordinates.y, clip_coordinates.z) / clip_coordinates.w;
        glm::dvec2 screen_coordinates = glm::dvec2{transforms.viewportV[2] / 2.0 * ndc_coordinates.x + transforms.viewportV[0] + transforms.viewportV[2] / 2.0,
            transforms.viewportV[3] / 2.0 * ndc_coordinates.y + transforms.viewportV[1] + transforms.viewportV[3] / 2.0};
        glm::dvec2 distorted_pixel = container->cam_ptr->getDistortedPixel(glm::dvec2{screen_coordinates.x, screen_coordinates.y});
        gsl_vector_set(f, i, error(pt, distorted_pixel));
        ++i;
    }
    return GSL_SUCCESS;
}

void Levenberg_Marquardt::callback(const size_t iter, void* params, const gsl_multifit_nlinear_workspace* w)
{
    gsl_vector* f = gsl_multifit_nlinear_residual(w);
    gsl_vector* x = gsl_multifit_nlinear_position(w);
    double rcond;

    // Compute reciprocal condition number of J(x)
    gsl_multifit_nlinear_rcond(&rcond, w);

    auto container = reinterpret_cast<Kernel::Container*>(params);

    container->log->message("iter %"
                            "\nfov = %"
                            "\ncx = %"
                            "\ncy = %"
                            "\nk1 = %"
                            "\ncond(J) = %"
                            "\n|f(x)| = %",
        std::to_string(iter),
        std::to_string(gsl_vector_get(x, 0)),
        std::to_string(gsl_vector_get(x, 1)),
        std::to_string(gsl_vector_get(x, 2)),
        std::to_string(gsl_vector_get(x, 3)),
        std::to_string(1.0 / rcond),
        std::to_string(gsl_blas_dnrm2(f)));
}

std::vector<double> Levenberg_Marquardt::optimize(const size_t nb_params, const std::vector<CalibrationPoint>& xs)
{
    const gsl_multifit_nlinear_type* T = gsl_multifit_nlinear_trust;
    gsl_multifit_nlinear_workspace* w;
    gsl_multifit_nlinear_fdf fdf;
    gsl_multifit_nlinear_parameters fdf_params = gsl_multifit_nlinear_default_parameters();
    const size_t nb_data = xs.size(); // nb of data pts to fit
    gsl_matrix* covar = gsl_matrix_alloc(nb_params, nb_params);
    gsl_vector* x = gsl_vector_alloc(nb_params);
    std::srand(std::time(nullptr));                                                               // initialize a random seed
    gsl_vector_set(x, 0, 50.0 + (static_cast<float>(std::rand()) / RAND_MAX * 2.0 - 1.0) * 25.0); // fov
    gsl_vector_set(x, 1, static_cast<float>(std::rand()) / RAND_MAX);                             // cx
    gsl_vector_set(x, 2, static_cast<float>(std::rand()) / RAND_MAX);                             // cy

    // set the initial guess for the distortion parameters
    for (size_t i = 3; i < nb_params - 6; ++i)
        gsl_vector_set(x, i, 0.0);

    for (size_t i = 0; i < 3; ++i)
    {
        gsl_vector_set(x, nb_params - 6 + i, 0.0);
        gsl_vector_set(x, nb_params - 3 + i, static_cast<float>(std::rand()) / RAND_MAX * M_PI * 2.0);
    }

    // Define the function to be minimized
    fdf.f = costFunct;
    fdf.df = nullptr;  // set to null for finite-difference Jacobian
    fdf.fvv = nullptr; // set to null -> no geodesic acceleration
    fdf.n = nb_data;
    fdf.p = nb_params;

    // Initialize a container to pass information to the cost function
    Kernel::Container container = Kernel::Container(_log, _camera_ptr, xs);
    fdf.params = (void*)(&container);

    w = gsl_multifit_nlinear_alloc(T, &fdf_params, nb_data, nb_params);

    gsl_multifit_nlinear_init(x, &fdf, w);

    // Compute initial cost function
    gsl_vector* f = gsl_multifit_nlinear_residual(w);
    double chisq, chisq0;
    gsl_blas_ddot(f, f, &chisq0);

    int info, status;
    const double xtol = 1e-8;
    const double gtol = 1e-8;
    const double ftol = 1e-8;

    status = gsl_multifit_nlinear_driver(1000, xtol, gtol, ftol, callback, nullptr, &info, w);
    if (status)
    {
        _log->error("Levenberg_Marquardt::optimize - An error has occured during minimization");
    }

    // Compute covariance of best fit parameters
    gsl_matrix* Jacobian = gsl_multifit_nlinear_jac(w);
    gsl_multifit_nlinear_covar(Jacobian, 0.0, covar);

    // Compute final cost
    gsl_blas_ddot(f, f, &chisq);

    double dof = nb_data - nb_params;
    _log->message("summary from method %/%"
                  "\nnumber of iterations: %"
                  "\nfunction evaluations: %"
                  "\nJacobian evaluations: %"
                  "\ninitial |f(x)| = %"
                  "\nfinal   |f(x)| = %"
                  "\nchisq / dof = ",
        gsl_multifit_nlinear_name(w),
        gsl_multifit_nlinear_trs_name(w),
        std::to_string(gsl_multifit_nlinear_niter(w)),
        std::to_string(fdf.nevalf),
        std::to_string(fdf.nevaldf),
        std::to_string(std::sqrt(chisq0)),
        std::to_string(std::sqrt(chisq)),
        std::to_string(chisq / dof));

    std::vector<double> selectedValues;
    std::vector<double> abs_error;
    double constant_var = GSL_MAX_DBL(1.0, std::sqrt(chisq / dof));
    for (size_t i = 0; i < nb_params; ++i)
    {
        selectedValues.push_back(gsl_vector_get(w->x, i));
        abs_error.push_back(constant_var * std::sqrt(gsl_matrix_get(covar, i, i)));
    }

    _log->message("fov       = % +/- %"
                  "cx        = % +/- %"
                  "cy        = % +/- %",
        std::to_string(selectedValues[0]),
        std::to_string(abs_error[0]),
        std::to_string(selectedValues[1]),
        std::to_string(abs_error[1]),
        std::to_string(selectedValues[2]),
        std::to_string(abs_error[2]));

    return selectedValues;
};

double Nelder_Mead::error(const CalibrationPoint& pt, const glm::dvec2& pixel, const double z)
{
    double distance = pt.weight * pow(pt.screen.x - pixel.x, 2.0) + pow(pt.screen.y - pixel.y, 2.0);
    return distance + penalty(z);
}

double Nelder_Mead::costFunc(const gsl_vector* v, void* params)
{
    if (params == nullptr)
        return 0.0;

    auto container = reinterpret_cast<Kernel::Container*>(params);
    double fov = gsl_vector_get(v, 0);
    double cx = gsl_vector_get(v, 1);
    double cy = gsl_vector_get(v, 2);

    // Some limits for the calibration parameters
    if (fov < 4.0 || fov > 120.0 || abs(cx - 0.5) > 1.0 || abs(cy - 0.5) > 1.0)
        return std::numeric_limits<double>::max();

    glm::dvec3 eye;
    glm::dvec3 euler;
    for (int i = 0; i < 3; ++i)
    {
        eye[i] = gsl_vector_get(v, v->size - 6 + i);
        euler[i] = gsl_vector_get(v, v->size - 3 + i);
    }

    std::vector<double> intrinsics;
    for (size_t i = 0; i < v->size - 6; ++i) // minus 6, we don't want the eye and euler angles
    {
        intrinsics.push_back(gsl_vector_get(v, i));
    }

    container->cam_ptr->updateFromParams(intrinsics);
    container->cam_ptr->updateExtrinsics(eye, euler);
    Camera::Transforms transforms(Camera::computeTransformationMatrices(container->cam_ptr->extrinsics(), container->cam_ptr->intrinsics()));

    // Project all the object points, and measure the distance between them and the image points
    double summedDistance = 0.0;
    for (const auto& pt : container->pts)
    {
        glm::dvec4 cam_coordinates = transforms.lookM * glm::dvec4(pt.world, 1.0);
        glm::dvec4 clip_coordinates = transforms.projM * cam_coordinates;
        glm::dvec3 ndc_coordinates = glm::dvec3(clip_coordinates.x, clip_coordinates.y, clip_coordinates.z) / clip_coordinates.w;
        glm::dvec2 screen_coordinates = glm::dvec2{transforms.viewportV[2] / 2.0 * ndc_coordinates.x + transforms.viewportV[0] + transforms.viewportV[2] / 2.0,
            transforms.viewportV[3] / 2.0 * ndc_coordinates.y + transforms.viewportV[1] + transforms.viewportV[3] / 2.0};
        glm::dvec2 distorted_pixel = container->cam_ptr->getDistortedPixel(glm::dvec2{screen_coordinates.x, screen_coordinates.y});
        summedDistance += error(pt, distorted_pixel, cam_coordinates.z);
    }
    summedDistance /= (container->pts).size();
    return summedDistance;
}

std::vector<double> Nelder_Mead::optimize(const size_t nb_params, const std::vector<CalibrationPoint>& xs)
{
    gsl_multimin_function calibrationFunc;
    calibrationFunc.n = nb_params;
    calibrationFunc.f = &costFunc;

    // Initialize a container to pass information to the cost function
    Kernel::Container container = Kernel::Container(_log, _camera_ptr, xs);
    calibrationFunc.params = (void*)(&container);
    std::srand(std::time(nullptr)); // initialize a random seed

    const gsl_multimin_fminimizer_type* minimizerType = gsl_multimin_fminimizer_nmsimplex2rand;
    // Variables we do not want to keep between tries
    glm::dvec3 eyeOriginal = _camera_ptr->extrinsics().eye;

    double minValue = std::numeric_limits<double>::max();
    std::vector<double> selectedValues(nb_params, std::numeric_limits<double>::max());

    gsl_multimin_fminimizer* minimizer;
    minimizer = gsl_multimin_fminimizer_alloc(minimizerType, nb_params);
    gsl_vector* x = gsl_vector_alloc(nb_params);
    gsl_vector* step = gsl_vector_alloc(nb_params);
    gsl_vector_set(step, 0, 10.0);
    gsl_vector_set(step, 1, 0.1);
    gsl_vector_set(step, 2, 0.1);

    // If any distortion factor, add a step size
    for (size_t i = 3; i < nb_params - 6; ++i)
        gsl_vector_set(step, i, 0.01);

    for (size_t i = 0; i < 3; ++i)
    {
        gsl_vector_set(step, nb_params - 6 + i, 1.0);
        gsl_vector_set(step, nb_params - 3 + i, M_PI / 4.0);
    }

    // First step: find a rough estimate, quickly
    for (double s = 0.0; s <= 1.3; s += 0.3)
    {
        for (double t = 0.0; t <= 1.3; t += 0.3)
        {
            gsl_vector_set(x, 0, 50.0 + (static_cast<float>(std::rand()) / RAND_MAX * 2.0 - 1.0) * 25.0); // fov
            gsl_vector_set(x, 1, s);                                                                      // cx
            gsl_vector_set(x, 2, t);                                                                      // cy

            // set the initial guess for the distortion parameters
            for (size_t i = 3; i < nb_params - 6; ++i)
                gsl_vector_set(x, i, 0.0);

            for (size_t i = 0; i < 3; ++i)
            {
                gsl_vector_set(x, nb_params - 6 + i, eyeOriginal[i]);
                gsl_vector_set(x, nb_params - 3 + i, static_cast<float>(std::rand()) / RAND_MAX * M_PI * 2.0);
            }

            gsl_multimin_fminimizer_set(minimizer, &calibrationFunc, x, step);

            size_t iter = 0;
            int status = GSL_CONTINUE;
            double localMinimum = std::numeric_limits<double>::max();
            while (status == GSL_CONTINUE && iter < 1000 && localMinimum > 64.0)
            {
                iter++;
                status = gsl_multimin_fminimizer_iterate(minimizer);
                if (status)
                {
                    _log->error("Nelder_Mead::optimize - An error has occured during minimization");
                    break;
                }

                status = gsl_multimin_test_size(minimizer->size, 1e-2);
                localMinimum = gsl_multimin_fminimizer_minimum(minimizer);
            }

            if (localMinimum < minValue)
            {
                minValue = localMinimum;
                for (size_t i = 0; i < selectedValues.size(); ++i)
                    selectedValues[i] = gsl_vector_get(minimizer->x, i);
            }
        }
    }

    // Second step: we improve on the best result from the previous step
    for (int index = 0; index < 8; ++index)
    {
        gsl_vector_set(step, 0, 1.0);  // fov
        gsl_vector_set(step, 1, 0.05); // cx
        gsl_vector_set(step, 2, 0.05); // cy

        for (size_t i = 3; i < nb_params - 6; ++i)
            gsl_vector_set(step, i, 0.01); // distortion parameters

        for (size_t i = 0; i < 3; ++i)
        {
            gsl_vector_set(step, nb_params - 6 + i, 0.1);         // eye
            gsl_vector_set(step, nb_params - 3 + i, M_PI / 10.0); // euler
        }

        for (size_t i = 0; i < selectedValues.size(); ++i)
            gsl_vector_set(x, i, selectedValues[i]);

        gsl_multimin_fminimizer_set(minimizer, &calibrationFunc, x, step);

        size_t iter = 0;
        int status = GSL_CONTINUE;
        double localMinimum = std::numeric_limits<double>::max();
        while (status == GSL_CONTINUE && iter < 10000 && localMinimum > 0.5)
        {
            iter++;
            status = gsl_multimin_fminimizer_iterate(minimizer);
            if (status)
            {
                _log->error("Nelder_Mead::optimize - An error has occured during minimization");
                break;
            }

            status = gsl_multimin_test_size(minimizer->size, 1e-7);
            localMinimum = gsl_multimin_fminimizer_minimum(minimizer);
        }

        if (localMinimum < minValue)
        {
            minValue = localMinimum;
            for (size_t i = 0; i < selectedValues.size(); ++i)
                selectedValues[i] = gsl_vector_get(minimizer->x, i);
        }
    }

    _log->message("Nelder_Mead:: - Minimum Value: %\n"
                  "\t- Values (fov, cx, cy): %, %, %",
        std::to_string(minValue),
        std::to_string(selectedValues[0]),
        std::to_string(selectedValues[1]),
        std::to_string(selectedValues[2]));

    gsl_vector_free(x);
    gsl_vector_free(step);
    gsl_multimin_fminimizer_free(minimizer);

    return selectedValues;
};
} // namespace calimiro
