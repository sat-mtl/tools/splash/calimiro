/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_WORKSPACE__
#define __CALIMIRO_WORKSPACE__

#include <cassert>
#include <filesystem>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>

#include <opencv2/opencv.hpp>

#include "./calimiro/constants.h"
#include "./calimiro/utils.h"

namespace calimiro
{

// Utility template, for use with static_assert
template <class T>
struct dependent_false : std::false_type
{
};

/**
 * Workspace class, responsible for reading/writing image and configurations files
 */
class Workspace
{
  public:
    using ImageList = std::vector<std::pair<std::string, cv::Mat>>;

  public:
    explicit Workspace(const std::string& path = calimiro::utils::getWorkspace())
        : _work_path(path)
    {
        std::filesystem::create_directory(_work_path);
    }

    /**
     * Get the work path for this workspace
     * \return Return the work path
     */
    std::string getWorkPath() const { return _work_path; }

    /**
     * Read all images from a paths list, relative to the work path
     * If template parameter is cv::Mat1b, images are read as grayscale
     * If template parameter is cv::Mat3b, images are read as colored
     * \param paths Vector of paths
     * \return Return an optional containing the vector of images if all went well
     */
    template <class T>
    std::optional<std::vector<T>> readImagesFromList(const std::vector<std::string>& paths)
    {
        const std::filesystem::path workpath(_work_path);
        std::vector<T> images;
        for (const auto& path : paths)
        {
            cv::Mat image;
            if constexpr (std::is_same<cv::Mat3b, T>::value)
                image = cv::imread((workpath / path).string(), cv::IMREAD_COLOR);
            else if constexpr (std::is_same<cv::Mat1b, T>::value)
                image = cv::imread((workpath / path).string(), cv::IMREAD_GRAYSCALE);
            else
                static_assert(dependent_false<T>::value, "Target image format must be cv::Mat1b or cv::Mat3b");

            if (!image.data)
                return {};
            images.emplace_back(image);
        }
        return images;
    }

    /**
     * Save images from a list given their path
     * \param images Vector of pairs of path + image to save
     * \return Return true if all went well
     */
    bool saveImagesFromList(const ImageList& images);

    /**
     * Generate the file paths for captured structured light patterns
     * The paths are ordered as follows:
     * position 0 -> projector 1 pattern 1
     *            -> projector 1 pattern 2
     *            -> ...
     *            -> projector 1 pattern n
     *            -> ...
     *            -> projector n pattern n
     * position 1 -> ...
     * ...
     * position n -> ...
     * \param position_count Number of capture positions
     * \param projector_count Number of projectors
     * \param pattern_size Number of frames for the structured light patterns
     * \return Return a vector containing the paths for all captures
     */
    std::vector<std::string> generateStructuredLightCapturePaths(size_t position_count, size_t projector_count, size_t pattern_size) const;

    /**
     * Generate the projector/image pixel correspondence from a vector of decoded patterns for each projector
     * \param decoded_projectors Vector of decoded patterns, one for each projector
     * \return Return the result of merging the decoded patterns into a single matrix
     */
    std::optional<cv::Mat3i> combineDecodedProjectors(const std::vector<cv::Mat2i>& decoded_projectors);

    /**
     * Export the given OpenCV matrix to a yml file
     * \param data OpenCV matrix to export
     * \param filepath Relative file path to export to
     * \param Return true if the matrix has been successfully exported
     */
    template <class T>
    bool exportMatrixToYaml(const T& data, const std::string& filepath)
    {
        static_assert(std::is_base_of<cv::Mat, T>::value == true);

        auto path = std::filesystem::path(_work_path) / filepath;
        std::filesystem::create_directories(path.parent_path());
        cv::FileStorage fs(std::string(path.replace_extension(".yml")), cv::FileStorage::WRITE);
        fs << path.stem().string() << data;
        return true;
    }

  private:
    std::string _work_path;
};

} // namespace calimiro

#endif // __CALIMIRO_WORKSPACE__
