/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./calimiro/pattern.h"

#include <cassert>
#include <fstream>
#include <iostream>

namespace calimiro
{

cv::Mat1b Structured_Light::computeShadowMask(const cv::Mat1b& black_image, const cv::Mat1b& white_image, size_t threshold)
{
    cv::Mat1b shadow_mask = cv::Mat1b::zeros(black_image.size());
    for (int i = 0; i < shadow_mask.rows; ++i)
    {
        for (int j = 0; j < shadow_mask.cols; ++j)
        {
            if (abs(white_image(i, j) - black_image(i, j)) > threshold)
            {
                shadow_mask(i, j) = 1;
            }
        }
    }
    return shadow_mask;
}

cv::Mat2i Structured_Light::decodeImage(const cv::Ptr<cv::structured_light::GrayCodePattern>& graycode, const std::vector<cv::Mat1b>& pattern, const cv::Mat1b& shadow_mask)
{
    cv::Mat2i decode_image = cv::Mat2i::zeros(shadow_mask.size());
    cv::Point proj_pixel;
    for (int i = 0; i < decode_image.rows; ++i)
    {
        for (int j = 0; j < decode_image.cols; ++j)
        {
            // if the pixel is not shadowed, reconstruct
            if (shadow_mask(i, j))
            {
                // for a (x,y) pixel of the camera returns the corresponding projector pixel by calculating the decimal number
                bool error = graycode->getProjPixel(pattern, j, i, proj_pixel);
                if (!error)
                {
                    decode_image(i, j)[0] = proj_pixel.x;
                    decode_image(i, j)[1] = proj_pixel.y;
                }
            }
        }
    }
    return decode_image;
}

std::optional<std::pair<cv::Mat1b, cv::Mat1b>> Structured_Light::getDecodedCoordinates(int width, int height) const
{
    if (!_decoded_image)
        return {};

    auto& decoded = _decoded_image.value();
    cv::Mat1b xMap = cv::Mat1b::zeros(decoded.size());
    cv::Mat1b yMap = cv::Mat1b::zeros(decoded.size());

    for (int i = 0; i < decoded.rows; ++i)
    {
        for (int j = 0; j < decoded.cols; ++j)
        {
            xMap(i, j) = decoded(i, j)[0] * 255.0f / (width * _scale);
            yMap(i, j) = decoded(i, j)[1] * 255.0f / (height * _scale);
        }
    }

    return std::make_pair(xMap, yMap);
}

std::optional<cv::Mat1b> Structured_Light::getShadowMask() const
{
    if (!_shadowmask)
        return {};

    const auto& shadowmask = _shadowmask.value();
    cv::Mat1b saturated = cv::Mat1b::zeros(shadowmask.size());
    for (int i = 0; i < shadowmask.rows; ++i)
        for (int j = 0; j < shadowmask.cols; ++j)
            if (shadowmask(i, j))
                saturated(i, j) = 255;
    return saturated;
}

std::vector<cv::Mat> Structured_Light::create(uint32_t width, uint32_t height)
{
    assert(width > 0 && height > 0);

    // Set up GrayCodePattern with parameters
    cv::structured_light::GrayCodePattern::Params params;
    params.width = width * _scale;
    params.height = height * _scale;
    cv::Ptr<cv::structured_light::GrayCodePattern> graycode = cv::structured_light::GrayCodePattern::create(params);

    // Storage for pattern
    std::vector<cv::Mat> pattern;
    graycode->generate(pattern);

    // Generate the white and black images needed for shadows mask computation
    cv::Mat white;
    cv::Mat black;
    graycode->getImagesForShadowMasks(black, white);

    pattern.push_back(white);
    pattern.push_back(black);

    cv::Mat p_resize;
    std::vector<cv::Mat> pattern_resize;
    for (const auto p : pattern)
    {
        cv::resize(p, p_resize, cv::Size(width, height), 0.0, 0.0, cv::INTER_NEAREST);
        pattern_resize.push_back(p_resize);
        p_resize.release();
    }
    _log->message("Pattern::Structured_Light::create - % pattern images successfully created", std::to_string(pattern.size()));
    return pattern_resize;
}

std::optional<cv::Mat2i> Structured_Light::decode(uint32_t width, uint32_t height, std::vector<cv::Mat1b> images)
{
    if (width == 0 && height == 0)
        return {};

    // Set up GraycodePattern with parameters
    cv::structured_light::GrayCodePattern::Params params;
    params.width = width * _scale;
    params.height = height * _scale;
    cv::Ptr<cv::structured_light::GrayCodePattern> graycode = cv::structured_light::GrayCodePattern::create(params);

    // Setting default values
    std::size_t white_thresh = 5;
    std::size_t black_thresh = 10;
    graycode->setWhiteThreshold(white_thresh);
    graycode->setBlackThreshold(black_thresh);

    auto nb_patterns = graycode->getNumberOfPatternImages();
    if (images.size() != nb_patterns + 2) // patterns + 2 for the shadow mask
        return {};

    if (images[0].type() != CV_8UC1)
        return {};

    _shadowmask = computeShadowMask(images[nb_patterns], images[nb_patterns + 1], black_thresh);
    _decoded_image = decodeImage(graycode, images, _shadowmask.value());
    return _decoded_image;
}

}; // namespace calimiro
