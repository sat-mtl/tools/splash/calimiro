/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_CALIMIRO__
#define __CALIMIRO_CALIMIRO__

#include "./calimiro/abstract_logger.h"
#include "./calimiro/camera.h"
#include "./calimiro/camera_model.h"
#include "./calimiro/estimator.h"
#include "./calimiro/geometry.h"
#include "./calimiro/io_obj.h"
#include "./calimiro/logger.h"
#include "./calimiro/mapXYZ.h"
#include "./calimiro/optimizer.h"
#include "./calimiro/pattern.h"
#include "./calimiro/reconstruction.h"
#include "./calimiro/utils.h"
#include "./calimiro/workspace.h"

#endif // __CALIMIRO_CALIMIRO__
