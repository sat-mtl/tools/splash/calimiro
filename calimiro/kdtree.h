/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_KDTREE__
#define __CALIMIRO_KDTREE__

#include <algorithm>
#include <iostream>
#include <memory> // for unique_ptr

#include "./calimiro/bqueue.h"

namespace calimiro
{

class Kdnode
{
  public:
    Kdnode(glm::vec3 p)
        : _point(p){};

  private:
    glm::vec3 _point;
    std::unique_ptr<Kdnode> _left = nullptr;
    std::unique_ptr<Kdnode> _right = nullptr;
    friend class Kdtree;
};

class Kdtree
{
  public:
    // Constructor
    Kdtree()
        : _root()
        , _sz(0){};

    // Accessor
    std::size_t size() { return _sz; };

    void insert(glm::vec3 point);
    std::vector<Bqueue<glm::vec3>::Elt> nearest_neighbors(const glm::vec3& point, int k);
    std::vector<double> nearest_neighbors_priorities(const glm::vec3& point, int k);

  private:
    std::unique_ptr<Kdnode> _root;
    std::size_t _sz;
    void _nearest_neighbors(Kdnode* current, const glm::vec3& point, Bqueue<glm::vec3>& bq, int split = 0);
};

// Implementation details

// Add one point at a time to the tree, the tree can become unbalanced.
void Kdtree::insert(glm::vec3 point)
{
    if (!_root)
    {
        _root = std::make_unique<Kdnode>(point);
        ++_sz;
        return;
    }

    Kdnode* current = _root.get();
    int split = 0;
    while (true)
    {
        if (point[split] < current->_point[split])
        {
            if (!current->_left)
            {
                current->_left = std::make_unique<Kdnode>(point);
                ++_sz;
                break;
            }
            else
            {
                current = current->_left.get();
            }
        }
        else
        {
            if (!current->_right)
            {
                current->_right = std::make_unique<Kdnode>(point);
                ++_sz;
                break;
            }
            else
            {
                current = current->_right.get();
            }
        }
        split = (split + 1) % 3;
    }
}

std::vector<Bqueue<glm::vec3>::Elt> Kdtree::nearest_neighbors(const glm::vec3& point, int k)
{
    Bqueue<glm::vec3> bq = Bqueue<glm::vec3>(k);
    _nearest_neighbors(_root.get(), point, bq);

    return bq.queue();
}

std::vector<double> Kdtree::nearest_neighbors_priorities(const glm::vec3& point, int k)
{
    Bqueue<glm::vec3> bq = Bqueue<glm::vec3>(k);
    _nearest_neighbors(_root.get(), point, bq);
    return bq.priorities();
}

void Kdtree::_nearest_neighbors(Kdnode* current, const glm::vec3& point, Bqueue<glm::vec3>& bq, int split)
{
    split %= 3;
    if (!current)
    {
        return;
    }

    double dist = glm::distance(current->_point, point);
    bq.insert(current->_point, dist);
    if (point[split] < current->_point[split])
    {
        if (current->_left)
        {
            // Recursively search the left subtree on the next axis
            _nearest_neighbors(current->_left.get(), point, bq, split + 1);
        }
    }
    else
    {
        if (current->_right)
        {
            // Recusively search the right subtree on the next axis
            _nearest_neighbors(current->_right.get(), point, bq, split + 1);
        }
    }
    // special case where a closest point might be on the opposite subtree or the bq is not at full capacity
    // ie the sphere crosses the splitting plane, we need to look on the other subtree
    if (abs(point[split] - current->_point[split]) < bq.peek().priority || bq.size() < bq.capacity())
    {
        if (point[split] < current->_point[split])
        {
            if (current->_right)
                _nearest_neighbors(current->_right.get(), point, bq, split + 1);
        }
        else
        {
            if (current->_left)
                _nearest_neighbors(current->_left.get(), point, bq, split + 1);
        }
    }
}

}; // namespace calimiro

#endif // __CALIMIRO_KDTREE__
