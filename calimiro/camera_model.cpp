/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./camera_model.h"

namespace calimiro
{
namespace cameramodel
{
/**
 * Implementation of ideal pinhole camera
 */
void Pinhole::updateFromParams(const std::vector<double>& params)
{
    assert(params.size() == 3);
    *this = Pinhole(_intrinsics.width, _intrinsics.height, params[0], params[1], params[2]);
}

/**
 * Implementation of an pinhole camera with one radial coefficient of distortion
 */
std::vector<double> Pinhole_Radial_k1::getParams() const
{
    std::vector<double> params = Pinhole::getParams();
    params.insert(params.end(), std::begin(_params), std::end(_params));
    return params;
};

glm::dvec2 Pinhole_Radial_k1::addDisto(const glm::dvec2& p) const
{
    const double k1 = _params[0];
    const double r2 = p.x * p.x + p.y * p.y;
    const double r_coeff = 1. + k1 * r2;

    return r_coeff * p;
};

glm::dvec2 Pinhole_Radial_k1::getDistortedPixel(const glm::dvec2& pixel) const
{
    return Camera::cam2ima(Pinhole_Radial_k1::addDisto(Camera::ima2cam(pixel)));
};

void Pinhole_Radial_k1::updateFromParams(const std::vector<double>& params)
{
    assert(params.size() == 4);
    *this = Pinhole_Radial_k1(_intrinsics.width, _intrinsics.height, params[0], params[1], params[2], params[3]);
}

/**
 * Implementation of an pinhole camera with three radial coefficients of distortion
 */
std::vector<double> Pinhole_Radial_k3::getParams() const
{
    std::vector<double> params = Pinhole::getParams();
    params.insert(params.end(), std::begin(_params), std::end(_params));
    return params;
};

glm::dvec2 Pinhole_Radial_k3::addDisto(const glm::dvec2& p) const
{
    const double k1 = _params[0];
    const double k2 = _params[1];
    const double k3 = _params[2];

    const double r2 = p.x * p.x + p.y * p.y;
    const double r_coeff = 1. + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2;

    return r_coeff * p;
};

glm::dvec2 Pinhole_Radial_k3::getDistortedPixel(const glm::dvec2& pixel) const
{
    return Camera::cam2ima(Pinhole_Radial_k3::addDisto(Camera::ima2cam(pixel)));
};

void Pinhole_Radial_k3::updateFromParams(const std::vector<double>& params)
{
    assert(params.size() == 6);
    *this = Pinhole_Radial_k3(_intrinsics.width, _intrinsics.height, params[0], params[1], params[2], params[3], params[4], params[5]);
}

} // namespace cameramodel
} // namespace calimiro
