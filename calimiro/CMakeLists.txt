#
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
#

include_directories(../)
include_directories(
        ${INSTANTMESHES_INCLUDE_DIRS}
        ${JSONCPP_INCLUDE_DIRS}
)

link_directories(${INSTANTMESHES_LIBRARY_DIRS})

#
# Target
#
add_library(calimiro-${API_VERSION} SHARED "")

target_sources(calimiro-${API_VERSION} PRIVATE
    calimiro.cpp
    camera.cpp
    camera_model.cpp
    estimator.cpp
    geometry.cpp
    io_obj.cpp
    mapXYZ.cpp
    optimizer.cpp
    pattern.cpp
    reconstruction.cpp
    texture_coordinates/texCoordUtils.cpp
    texture_coordinates/texCoordGen.cpp
    texture_coordinates/texCoordGen_domemaster.cpp
    texture_coordinates/texCoordGen_equirectangular.cpp
    texture_coordinates/texCoordGen_orthographic.cpp
    texture_coordinates/texCoordGen_planar.cpp
    texture_coordinates/texCoordGen_spheric.cpp
    utils.cpp
    workspace.cpp
    ../external/easyexif/exif.cpp
)
add_executable(calimiro calimiro.cpp)

target_link_libraries(calimiro calimiro-${API_VERSION})

#
# Link flags
#
target_link_libraries(calimiro-${API_VERSION}
    stdc++fs  #required for std::filesystem
    GSL::gsl
    ${JSONCPP_LIBRARIES}
    ${OpenCV_LIBS}
    ${INSTANTMESHES_LIBRARY_DIRS}/libinstant-meshes.a
    ${INSTANTMESHES_LIBRARY_DIRS}/libtbb_static.a
    dl  # should be after instant-meshes
    pthread  # should be after instant-meshes
    OpenMVG::openMVG_camera
    OpenMVG::openMVG_exif
    OpenMVG::openMVG_features
    OpenMVG::openMVG_matching
    OpenMVG::openMVG_matching_image_collection
    OpenMVG::openMVG_sfm
    OpenMVG::openMVG_system
    pmp-lib
    )

set(HEADER_INCLUDES
    calimiro.h
    abstract_logger.h
    camera.h
    camera_model.h
    calibration_point.h
    config.h
    logger.h
    constants.h
    estimator.h
    geometry.h
    io_obj.h
    mapXYZ.h
    optimizer.h
    pattern.h
    reconstruction.h
    utils.h
    workspace.h
    )

set(HEADER_INCLUDES_TEXCOORD
    texture_coordinates/texCoordUtils.h
    texture_coordinates/texCoordGen.h
    texture_coordinates/texCoordGen_domemaster.h
    texture_coordinates/texCoordGen_equirectangular.h
    texture_coordinates/texCoordGen_orthographic.h
    texture_coordinates/texCoordGen_planar.h
    texture_coordinates/texCoordGen_spheric.h
    )

# Enable all warnings as errors
#target_compile_options(calimiro-${API_VERSION} PRIVATE -Wall -Wextra -Werror)

#
# Pkg-config
#
if (UNIX)
    include(FindPkgConfig QUIET)
    if (PKG_CONFIG_FOUND)
        configure_file("calimiro.pc.in" "calimiro-${API_VERSION}.pc" @ONLY)
        install(FILES "${CMAKE_CURRENT_BINARY_DIR}/calimiro-${API_VERSION}.pc"
            DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig")
    endif ()
endif ()

#
# Installation
#
install(TARGETS calimiro-${API_VERSION}
    LIBRARY DESTINATION "lib/"
    COMPONENT libraries
    )

install(FILES ${HEADER_INCLUDES}
    DESTINATION "include/calimiro-${API_VERSION}/calimiro"
    COMPONENT headers)

install(FILES ${HEADER_INCLUDES_TEXCOORD}
    DESTINATION "include/calimiro-${API_VERSION}/calimiro/texture_coordinates"
    COMPONENT headers)
