/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_OBJ__
#define __CALIMIRO_OBJ__

#include <vector>

#include "./calimiro/abstract_logger.h"
#include "./calimiro/geometry.h"

namespace calimiro
{

class Obj : public Geometry
{
  public:
    Obj(AbstractLogger* log)
        : Geometry::Geometry(log){};
    Obj(AbstractLogger* log, Geometry g)
        : Geometry::Geometry(log, g.vertices(), g.normals(), g.uvs(), g.faces()){};
    bool writeMesh(const std::string& filename);
    bool readMesh(const std::string& filename);

  private:
    glm::vec3 loadCoordinates(std::string line);
    std::vector<FaceVertex> loadFaces(std::string line);
};

}; // namespace calimiro

#endif // __CALIMIRO_OBJ__
