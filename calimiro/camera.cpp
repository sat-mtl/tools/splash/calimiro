/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./camera.h"

#include <algorithm>

#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>

namespace calimiro
{

glm::dvec2 Camera::toNDC(glm::dvec2 screen, double width, double height, double cx, double cy)
{
    double normalization_factor = std::max(width, height);
    glm::dvec2 center{cx * width, cy * height};
    glm::dvec2 normalized{2.0 * (screen.x - center.x) / normalization_factor, 2.0 * (screen.y - center.y) / normalization_factor};
    return normalized;
};

glm::dvec2 Camera::toScreen(glm::dvec2 normalized, double width, double height, double cx, double cy)
{
    double normalization_factor = std::max(width, height);
    glm::dvec2 center{cx * width, cy * height};
    glm::dvec2 screen{center.x + (normalization_factor / 2.0 * normalized.x), center.y + (normalization_factor / 2.0 * normalized.y)};
    return screen;
};

glm::dmat4 Camera::computeProjectionMatrix(const Camera::Intrinsics& intrinsics)
{
    double l, r, t, b, n, f;
    n = intrinsics.near;
    f = intrinsics.far;
    // Up and down
    double tTemp = n * std::tan(intrinsics.fov * M_PI / 360.0);
    t = tTemp - (intrinsics.cy - 0.5) * 2.0 * tTemp;
    b = -tTemp - (intrinsics.cy - 0.5) * 2.0 * tTemp;
    // Left and right
    double rTemp = tTemp * intrinsics.width / intrinsics.height;
    double lTemp = -tTemp * intrinsics.width / intrinsics.height;
    r = rTemp - (intrinsics.cx - 0.5) * (rTemp - lTemp);
    l = lTemp - (intrinsics.cx - 0.5) * (rTemp - lTemp);
    return glm::frustum(l, r, b, t, n, f);
}

Camera::Extrinsics Camera::computeExtrinsics(const glm::dvec3 eye, const glm::dvec3 euler)
{
    glm::dmat4 rotateMat = glm::yawPitchRoll(euler[0], euler[1], euler[2]);
    glm::dvec4 tmp_target = rotateMat * glm::dvec4(1.0, 0.0, 0.0, 0.0);
    glm::dvec4 tmp_up = rotateMat * glm::dvec4(0.0, 0.0, 1.0, 0.0);
    glm::dvec3 target{tmp_target[0], tmp_target[1], tmp_target[2]};
    glm::dvec3 up{tmp_up[0], tmp_up[1], tmp_up[2]};
    target += eye;
    up = normalize(up);
    return Camera::Extrinsics({eye, target, up});
}

Camera::Transforms Camera::computeTransformationMatrices(const Camera::Extrinsics& extrinsics, const Camera::Intrinsics& intrinsics)
{
    glm::dmat4 lookM = lookAt(extrinsics.eye, extrinsics.target, extrinsics.up);
    glm::dmat4 projM = glm::dmat4(Camera::computeProjectionMatrix(intrinsics));
    glm::dvec4 viewportV(0, 0, intrinsics.width, intrinsics.height);
    return Camera::Transforms({lookM, projM, viewportV});
}

glm::dvec2 Camera::computeProjection(const Camera::Transforms& transform, const glm::dvec3& world_pt)
{
    glm::dvec4 cam_coordinates = transform.lookM * glm::dvec4(world_pt, 1.0);
    glm::dvec4 clip_coordinates = transform.projM * cam_coordinates;
    glm::dvec3 ndc_coordinates = glm::dvec3(clip_coordinates.x, clip_coordinates.y, clip_coordinates.z) / clip_coordinates.w;
    glm::dvec2 screen_coordinates = glm::dvec2{transform.viewportV[2] / 2.0 * ndc_coordinates.x + transform.viewportV[0] + transform.viewportV[2] / 2.0,
        transform.viewportV[3] / 2.0 * ndc_coordinates.y + transform.viewportV[1] + transform.viewportV[3] / 2.0};
    return screen_coordinates;
}

glm::dvec2 Camera::ima2cam(const glm::dvec2& p) const
{
    glm::dvec2 cam_pt = Camera::toNDC(p, _intrinsics.width, _intrinsics.height, _intrinsics.cx, _intrinsics.cy);
    return cam_pt;
};

glm::dvec2 Camera::cam2ima(const glm::dvec2& p) const
{
    glm::dvec2 screen_pt = Camera::toScreen(p, _intrinsics.width, _intrinsics.height, _intrinsics.cx, _intrinsics.cy);
    return screen_pt;
};

} // namespace calimiro
