/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_TEXTURE_COORDINATES_UTILS__
#define __CALIMIRO_TEXTURE_COORDINATES_UTILS__

#include <map>
#include <vector>

#include "./calimiro/texture_coordinates/texCoordGen.h"
#include "./calimiro/texture_coordinates/texCoordGen_domemaster.h"
#include "./calimiro/texture_coordinates/texCoordGen_equirectangular.h"
#include "./calimiro/texture_coordinates/texCoordGen_orthographic.h"
#include "./calimiro/texture_coordinates/texCoordGen_planar.h"
#include "./calimiro/texture_coordinates/texCoordGen_spheric.h"

namespace calimiro
{

class TexCoordUtils
{
  public:
    enum texCoordMethod
    {
        DOMEMASTER,
        EQUIRECTANGULAR,
        ORTHOGRAPHIC,
        PLANAR,
        SPHERIC
    };

    /**
     * Getter for the list of available automatic texture coordinate (uv) generation methods.
     * \return vector of texCoordMethod (enum).
     */
    static const std::vector<texCoordMethod> getTexCoordMethodList();

    /**
     * Getter for a texture coordinate (uv) generation method "title" / its string friendly name (i.e. to display in a menu).
     * \param method the method to get the name of.
     * \return the string friendly name of the method.
     */
    static const std::string getTexCoordMethodTitle(texCoordMethod method);

    /**
     * Getter for a texture coordinate (uv) generation method description.
     * \param method the method to get the description of.
     * \return the description of the method as a string.
     */
    static const std::string getTexCoordMethodDescription(texCoordMethod method);

  private:
    static std::vector<texCoordMethod> _texCoordMethodsList;
    static std::map<const texCoordMethod, const std::string> _texCoordMethodsTitles;
    static std::map<const texCoordMethod, const std::string> _texCoordMethodsDescriptions;
};

} // namespace calimiro

#endif // __CALIMIRO_TEXTURE_COORDINATES_UTILS__
