/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./calimiro/texture_coordinates/texCoordUtils.h"

namespace calimiro
{
std::vector<TexCoordUtils::texCoordMethod> TexCoordUtils::_texCoordMethodsList = {
    texCoordMethod::PLANAR, texCoordMethod::ORTHOGRAPHIC, texCoordMethod::SPHERIC, texCoordMethod::EQUIRECTANGULAR, texCoordMethod::DOMEMASTER};

std::map<const TexCoordUtils::texCoordMethod, const std::string> TexCoordUtils::_texCoordMethodsTitles = {{texCoordMethod::DOMEMASTER, "Dome master projection"},
    {texCoordMethod::EQUIRECTANGULAR, "Equirectangular projection"},
    {texCoordMethod::ORTHOGRAPHIC, "Orthographic projection"},
    {texCoordMethod::PLANAR, "Planar projection"},
    {texCoordMethod::SPHERIC, "Spherical projection"}};

std::map<const TexCoordUtils::texCoordMethod, const std::string> TexCoordUtils::_texCoordMethodsDescriptions = {
    {texCoordMethod::DOMEMASTER, "Projection of a dome on a 2d plane where the pole is in the center."},
    {texCoordMethod::EQUIRECTANGULAR, "Projection of a sphere on a 2d plane similar as the way the Earth would be displayed on a map."},
    {texCoordMethod::ORTHOGRAPHIC, "Projection of the 3d mesh on a 2d plane following eye orientation. The projection is orthogonal."},
    {texCoordMethod::PLANAR, "Projection of the 3d mesh on a 2d plane following eye orientation using perspective from eye position."},
    {texCoordMethod::SPHERIC, "Projection of the 3d mesh of a dome or sphere on a 2d plane following eye orientation."}};

const std::vector<TexCoordUtils::texCoordMethod> TexCoordUtils::getTexCoordMethodList()
{
    return _texCoordMethodsList;
}

const std::string TexCoordUtils::getTexCoordMethodTitle(TexCoordUtils::texCoordMethod method)
{
    return _texCoordMethodsTitles[method];
}

const std::string TexCoordUtils::getTexCoordMethodDescription(TexCoordUtils::texCoordMethod method)
{
    return _texCoordMethodsDescriptions[method];
}

} // namespace calimiro
