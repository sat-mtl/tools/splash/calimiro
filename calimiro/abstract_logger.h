/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

// This header is a copy of shmdata abstract-logger.hpp.
// The library can be found here: https://gitlab.com/sat-metalab/shmdata/tree/master

#ifndef __CALIMIRO_ABSTRACT_LOGGER__
#define __CALIMIRO_ABSTRACT_LOGGER__

#include <string>

#define CALIMIRO_MakeLevel(NAME)                                                                                                                                                   \
  public:                                                                                                                                                                          \
    template <typename... Targs>                                                                                                                                                   \
    void NAME(const char* format, const ::std::string& value, Targs... Fargs)                                                                                                      \
    {                                                                                                                                                                              \
        on_##NAME(make_string(format, std::forward<const std::string&>(value), std::forward<Targs>(Fargs)...));                                                                    \
    }                                                                                                                                                                              \
    void NAME(const char* format) { on_##NAME(make_string(format)); }                                                                                                              \
                                                                                                                                                                                   \
  private:                                                                                                                                                                         \
    virtual void on_##NAME(std::string&&) = 0;

namespace calimiro
{

class AbstractLogger
{
  public:
    virtual ~AbstractLogger() = default;
    CALIMIRO_MakeLevel(error);
    CALIMIRO_MakeLevel(critical);
    CALIMIRO_MakeLevel(warning);
    CALIMIRO_MakeLevel(message);
    CALIMIRO_MakeLevel(info);
    CALIMIRO_MakeLevel(debug);

  private:
    std::string make_string(const char* format) { return std::string(format); }
    template <typename... Targs>
    std::string make_string(const char* format, const std::string& value, Targs... Fargs)
    {
        std::string res;
        for (; *format != '\0'; format++)
        {
            if (*format == '%')
            {
                res.append(value);
                return res.append(make_string(format + 1, Fargs...));
            }
            res.append(format, 1);
        }
        return res;
    }
};

} // namespace calimiro

#endif // __CALIMIRO_ABSTRACT_LOGGER__
