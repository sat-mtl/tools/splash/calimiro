calimiro release notes
======================
Here you will find a high level list of new features and bugfixes for each releases.

calimiro 0.2.0 (2021-03-08)
---------------------------

New features:
* ✨ added computations for new types of texture coordinates

Improvements:
* 🔨 added flag to enable/disable architecture optimization
* 🔨 added missing git-lfs instructions
* ♻️ changed std::vector<std::vector<double>> to glm::vec3

Bug Fixes:
* 🐛 fixed compile options not being enforced
* 🐛 fixed Meshlab on 20.04
* 🐛 fixed release script not displaying commit messages

calimiro 0.1.0 (2020-10-06)
---------------------------

New features:
* ✨ added a wrapper around MeshLab Marching Cubes
* ✨ pmp-lib now takes care of the mesh simplification
* ✨ added a camera model with 3 distortion parameters
* ✨ added a release script
* ✨ added shmdata's logger to slaps
* 🍱 added calimiro logo
* 🔨 added a setup script
* ⚡️ added optimization flags
* 👷 added gitlab CI support
* ✅ added coverage test

Improvements:
* 💥 renamed slaps to calimiro
* 📄 changed the license from LGPL to GPL
* 👷 upgraded the CI to Ubuntu 20.04

Bug Fixes:
* 🐛 fixed sfm data not being updated after incremental SfM
* 🐛 fixed a rounding error in the structured light pattern generation
* 🐛 fixed issues preventing using slaps as an external lib
* 🐛 fixed the bug in reading and writing obj files
