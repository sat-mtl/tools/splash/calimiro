#!/usr/bin/env bash

SOURCE_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
EXTERNAL_DIR="${SOURCE_DIR}/external"
INSTANT_MESHES_SRC_DIR="${EXTERNAL_DIR}/instant-meshes"

set -e

SUDO=''

check_sudo()
{
    if ((${EUID} != 0)); then
        SUDO='sudo'
    fi
}

update_git()
{
  echo "Updating submodules..."
  git submodule update --init --recursive

  echo "Fetching LFS..."
  git lfs fetch && git lfs pull
}

build_instant-meshes()
{
  echo "Building instant-meshes..."

  pushd ${INSTANT_MESHES_SRC_DIR}

  rm -f CMakeCache.txt
  cmake -DCMAKE_INSTALL_PREFIX=${EXTERNAL_DIR}/third_parties -DCMAKE_BUILD_TYPE=Release -DINSTANT_MESHES_BUILD_LIB=ON -DINSTANT_MESHES_BUILD_STATIC=ON .
  make -j$(nproc)
  make install

  popd
  echo "Finished building instant-meshes."
}

build_openMVG()
{
  echo "Building OpenMVG..."

  rm -rf build && mkdir build
  pushd build

  cmake ..
  # This call also installs openMVG in external/third_parties
  make -j$(nproc) openMVG-external

  popd
}

build_deps()
{
  build_instant-meshes
  build_openMVG
}

cleanup()
{
  rm -rf build
}

check_sudo
update_git
build_deps
cleanup
