#!/bin/bash

AUTHORS="./AUTHORS.md"
if ! [ -f "$AUTHORS" ]
then
    AUTHORS="../AUTHORS.md"
    if ! [ -f "$AUTHORS" ]
    then
	echo "no authors file found, exiting"
	exit
    fi
fi

# order by number of commits
# git shortlog -e -s -n | \
git log --format='%ae' | \
    grep -v metalab | \
    sort | \
    sed 's/marie-eve.dumas@mail.mcgill.ca/Marie-Eve Dumas/' | \
    sed 's/mdumas@sat.qc.ca/Marie-Eve Dumas/' | \
    sed 's/edurand@sat.qc.ca/Emmanuel Durand/' | \
    sed 's/emmanueldurand@gmail.com/Emmanuel Durand/' | \
    sed 's/frederic.lestage@gmail.com/Frédéric Lestage/' | \
    uniq -c | \
    sort -bgr | \
    sed 's/\ *[0-9]*\ /\* /' > ${AUTHORS}

