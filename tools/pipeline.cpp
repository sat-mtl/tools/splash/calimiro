/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h> // Required by libgphoto2 to access files
#include <filesystem>
#include <iostream>

#include <gphoto2/gphoto2.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include "./calimiro/logger.h"
#include "./calimiro/pattern.h"
#include "./calimiro/reconstruction.h"
#include "./calimiro/utils.h"
#include "./calimiro/workspace.h"

static const char* keys = {"{@proj_number     | | Number of projector        }"
                           "{@scale           | | Scale factor               }"
                           "{@proj_width      | | Projector width            }"
                           "{@proj_height     | | Projector height           }"
                           "{@nb_position     | | Number of position         }"};

static void Help()
{
    std::cout << "\nThis example shows how to use the \"Structured Light module\" to acquire a graycode pattern\n"
              << "./pipeline <proj_number> <scale> <proj_width> <proj_height> <nb_position>\n"
              << "Note that the camera must be in manual mode and the autofocus should be off\n"
              << std::endl;
}

// from https://github.com/gphoto/libgphoto2/blob/master/examples/config.c
int CanonEnableCapture(Camera* camera, int onoff, GPContext* context)
{
    CameraWidget *widget = nullptr, *child = nullptr;
    CameraWidgetType type;
    int ret;

    ret = gp_camera_get_single_config(camera, "capture", &widget, context);
    if (ret < GP_OK)
    {
        std::cerr << "libgphoto2::camera_get_config failed" << std::endl;
        return ret;
    }

    ret = gp_widget_get_type(child, &type);
    if (ret < GP_OK)
    {
        std::cerr << "libgphoto2::widget get type failed" << std::endl;
        gp_widget_free(widget);
        return ret;
    }

    switch (type)
    {
    case GP_WIDGET_TOGGLE:
        break;
    default:
        std::cerr << "libgphoto2::widget has bad type" << std::endl;
        ret = GP_ERROR_BAD_PARAMETERS;
        gp_widget_free(widget);
        return ret;
    }
    // Set the toggle to the wanted value
    ret = gp_widget_set_value(child, &onoff);
    if (ret < GP_OK)
    {
        std::cerr << "libgphoto2::toggling Canon capture to" << onoff << "failed" << std::endl;
        gp_widget_free(widget);
        return ret;
    }

    ret = gp_camera_set_single_config(camera, "capture", widget, context);
    if (ret < GP_OK)
    {
        std::cerr << "libgphoto2::camera_set_config failed" << std::endl;
        return ret;
    }

    return ret;
}

bool InitCamera(GPContext* context, Camera** camera)
{
    context = gp_context_new();
    gp_camera_new(camera);
    std::cout << "libgphoto2::Initialisation of the camera" << std::endl;
    if (gp_camera_init(*camera, context) < GP_OK)
    {
        std::cout << "libgphoto2::Camera initialisation failed" << std::endl;
        gp_camera_exit(*camera, context);
        return false;
    }
    return true;
}

cv::Mat Capture(GPContext* context, Camera* camera, bool write_metadata)
{
    CameraFilePath camera_file_path{};
    cv::Mat img;

    if (gp_camera_capture(camera, GP_CAPTURE_IMAGE, &camera_file_path, context) == GP_OK)
    {
        if (std::string(camera_file_path.name).find(".jpg") != std::string::npos || std::string(camera_file_path.name).find(".JPG") != std::string::npos)
        {
            CameraFile* destination;
            int handle = open((std::filesystem::path("/tmp") / camera_file_path.name).c_str(), O_CREAT | O_WRONLY, 0666);
            if (handle != -1)
            {
                gp_file_new_from_fd(&destination, handle);
                gp_camera_file_get(camera, camera_file_path.folder, camera_file_path.name, GP_FILE_TYPE_NORMAL, destination, context);
                gp_file_free(destination);
            }

            if (write_metadata)
            {
                calimiro::utils::writeMetadata(std::filesystem::path("/tmp") / camera_file_path.name);
            }

            img = cv::imread((std::filesystem::path("/tmp") / camera_file_path.name).string());
            if (img.empty())
            {
                std::cout << "libgphoto2::"
                          << " - cv::imread(): image not found" << std::endl;
            }
        }
        else
        {
            std::cout << "libgphoto2::"
                      << " - Captured image filetype is not jpeg."
                      << "Maybe the camera is set to RAW?" << std::endl;
        }
        gp_camera_file_delete(camera, camera_file_path.folder, camera_file_path.name, context);
        // Delete the file
        if (remove((std::filesystem::path("/tmp") / camera_file_path.name).c_str()) == -1)
            std::cout << "libghoto2::"
                      << " - Unable to delete file /tmp/" << camera_file_path.name << std::endl;
    }

    return img;
}

int main(int argc, char** argv)
{
    cv::CommandLineParser parser(argc, argv, keys);
    int nb_prj = parser.get<int>(0);
    float scale = parser.get<float>(1);
    int width = parser.get<int>(2);
    int height = parser.get<int>(3);
    int nb_pos = parser.get<int>(4);

    // verify the validity of the input
    if (nb_prj < 1 || scale < 0.0 || width < 1 || height < 1 || nb_pos < 1 || argc != 6)
    {
        Help();
        return -1;
    }

    calimiro::Logger logger;
    calimiro::Structured_Light structure_l(&logger, scale);
    auto patterns = structure_l.create(1920, 1080);

    calimiro::Workspace workspace;
    auto capture_paths = workspace.generateStructuredLightCapturePaths(nb_pos, nb_prj, patterns.size());

    // Open camera, using libgphoto2
    GPContext* context{nullptr};
    Camera* camera{nullptr};

    // Setting pattern window on projector monitor
    cv::namedWindow("Pattern Window", cv::WINDOW_NORMAL);

    if (!InitCamera(context, &camera))
    {
        std::cout << "libgphoto2::"
                  << " - No camera detected.\n"
                  << std::endl;
        return -1;
    }

    // Call to enable/disable the specific canon capture mode
    // not required for non canons
    CanonEnableCapture(camera, TRUE, context);

    auto capture_paths_it = capture_paths.cbegin();
    for (int k = 0; k < nb_pos; ++k)
    {
        std::cout << "Position " << k + 1 << std::endl;
        std::cout << "*************" << std::endl;
        for (int j = 0; j < nb_prj; ++j)
        {
            cv::moveWindow("Pattern Window", (j * (width - 1)), 0);
            cv::setWindowProperty("Pattern Window", cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);
            int i = 0;
            std::cout << "Projector " << j + 1 << std::endl;
            std::cout << "*************" << std::endl;

            while (i < patterns.size())
            {
                bool write_metadata = i == 0 ? 1 : 0;
                cv::imshow("Pattern Window", patterns[i]);
                cv::waitKey(180);

                cv::Mat frame = Capture(context, camera, write_metadata);
                if (frame.data)
                {
                    if (workspace.saveImagesFromList({{*(capture_paths_it++), frame}}))
                    {
                        std::cout << "Pattern " << i + 1 << " saved" << std::endl << std::endl;
                        ++i;
                    }
                    else
                    {
                        std::cout << "Pattern " << i + 1 << " NOT saved" << std::endl << std::endl << "Retry, checkthe path" << std::endl << std::endl;
                    }
                }
                else
                {
                    std::cout << "No frame data, waiting for new frame" << std::endl;
                }
            }
        }
        // change the position of the camera, then press key to continue the process
        // show a green screen to see the window location
        cv::imshow("Pattern Window", cv::Mat(patterns[0].size(), CV_8UC3, cv::Scalar(0, 255, 0)));
        cv::waitKey(0);
    }
    // Close the camera
    gp_camera_exit(camera, context);
    gp_camera_free(camera);
    gp_context_unref(context);

    // Decode the structured light for each projector at each position
    capture_paths_it = capture_paths.cbegin();
    for (size_t position = 0; position < nb_pos; ++position)
    {
        std::vector<cv::Mat2i> decoded_projectors;
        for (size_t projector = 0; projector < nb_prj; ++projector)
        {
            std::vector<std::string> image_paths;
            for (size_t i = 0; i < patterns.size(); ++i)
                image_paths.emplace_back(*(capture_paths_it++));

            auto images = workspace.readImagesFromList<cv::Mat1b>(image_paths);
            if (!images)
            {
                std::cout << "Unable to read saved captures, exiting" << std::endl;
                exit(1);
            }

            auto decoded = structure_l.decode(1920, 1080, images.value());
            if (!decoded)
            {
                std::cout << "Unable to decode capture from projector " << projector << " at position " << position << std::endl;
                exit(1);
            }

            auto shadowmask = structure_l.getShadowMask();
            auto decoded_coordinates = structure_l.getDecodedCoordinates(width, height);
            calimiro::Workspace::ImageList images_to_save(
                {{"decoded_images/pos_" + std::to_string(position) + "_proj" + std::to_string(projector) + "_shadow_mask.jpg", shadowmask.value()},
                    {"decoded_images/pos_" + std::to_string(position) + "_proj" + std::to_string(projector) + "_x.jpg", decoded_coordinates.value().first},
                    {"decoded_images/pos_" + std::to_string(position) + "_proj" + std::to_string(projector) + "_y.jpg", decoded_coordinates.value().second}});
            workspace.saveImagesFromList(images_to_save);

            decoded_projectors.push_back(decoded.value());
        }

        auto merged_projectors = workspace.combineDecodedProjectors(decoded_projectors);
        if (!merged_projectors)
        {
            std::cout << "Unable to merge decoded captures for position " << position << std::endl;
            exit(1);
        }

        if (!workspace.exportMatrixToYaml(merged_projectors.value(), "decoded_matrix/pos_" + std::to_string(position)))
        {
            std::cout << "Unable to export projector data for position " << position << std::endl;
            exit(1);
        }
    }

    calimiro::Reconstruction rec = calimiro::Reconstruction(&logger);
    rec.sfmInitImageListing();
    rec.computeFeatures();
    rec.computeMatches();
    rec.incrementalSfM();
    rec.computeStructureFromKnownPoses();
    rec.convertSfMStructure();

    return 0;
}
