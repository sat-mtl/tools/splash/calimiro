Contributing
============

Repositories
------------

The main repository for Calimiro is located on [Gitlab](https://gitlab.com/splashmapper/calimiro).


Coding style
------------

We use LLVM style, with a few exceptions. See the [clang-format configuration](./.clang-format) for more about this.

It is possible to let git ensure that you are conforming to the standards by using pre-commit hooks and clang-format:
```
sudo apt-get install clang-format exuberant-ctags
# Then in Calimiro's root folder:
rm -rf .git/hooks && ln -s $(pwd)/.hooks $(pwd)/.git/hooks
```

Note that you may have on some distribution to create a symlink from `clang-format-x.x` to `clang-format`. For example, on Ubuntu 18.04:
```
sudo ln -s /usr/bin/clang-format-6.0 /usr/bin/clang-format
```

Contributing
------------

Please send your pull request at the [SAT-Metalab's Gitlab repository](https://gitlab.com/splashmapper/calimiro). If you do not know how to make a pull request, Gitlab provides some [help about collaborating on projects using issues and pull requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

Branching strategy with git
---------------------------

The [master](https://gitlab.com/splashmapper/calimiro/tree/master) branch contains Calimiro releases. Validated new developments are into the [develop](https://gitlab.com/sat-metalab/calimiro/tree/develop) branch.

Modifications are made into a dedicated branch that needs to be merged into the develop branch through a Gitlab merge request. When you modification is ready, you need to prepare your merge request as follow:

* Update your develop branch.
```
git fetch
git checkout develop
git pull origin develop
```
* Go back to your branch and rebase onto develop. Note that it would be appreciated that you only merge request from a single commit (i.e interactive rebase with squash and/or fixup before pushing).
```
git checkout NAME_OF_YOUR_BRANCH
git rebase -i develop
```

Debugging
---------

When debugging, the first step is to compile `Calimiro` in `Debug` mode. From the build directory, run:
```bash
cmake -DCMAKE_BUILD_TYPE=Debug ..
```
You can then use [`gdb`](https://www.gnu.org/software/gdb/) to debug the failing script.

How to make new releases
------------------------

- The python [release script](./tools/release_version.py) takes you through the steps for release.
- Before running the script, your repo must have a default editor. Run the following command to select a core editor for the git repo:
    ```sh
    git config core.editor "<your_favorite_texteditor(vim/emacs/nano...)>"
    # use the '--global' flag to set the editor as default for all repos
    ```
- Run the release script as follows and follow the prompt:
    ```sh
    ./tools/release_version.py
    ```
    The script will create a new directory, clone the repo, build and run tests locally before actually making the release. This may take a long time.
- Before your new release is ready to be pushed upstream, the script will open the [NEWS.md](./NEWS.md) on the editor you previously set, with all the merge commits since the last tag. Edit as required, save and close the text editor.
