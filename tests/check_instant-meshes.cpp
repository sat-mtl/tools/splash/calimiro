/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <chrono>
#include <filesystem>
#include <iostream>

#include "./calimiro/io_obj.h"
#include "./calimiro/logger.h"

namespace fs = std::filesystem;

using namespace calimiro;

int main()
{
    Logger logger;
    auto start = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    auto obj = Obj(&logger);
    if (!obj.readMesh(fs::current_path() / "data" / "semidome.obj"))
    {
        std::cout << "Error: readMesh failed" << std::endl;
        return 1;
    }
    obj.simplifyGeometry();
    auto end = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    std::cout << "Geometry simplification took: " << (end - start) << "\n";

    return 0;
}
