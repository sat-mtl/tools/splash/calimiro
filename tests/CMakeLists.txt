#
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
#

include_directories(../)

link_libraries(${CALIMIRO_LIBRARY})
#
# Tests
#

add_executable(check_bqueue check_bqueue.cpp)
add_test(check_bqueue check_bqueue)

add_executable(check_kdtree check_kdtree.cpp)
add_test(check_kdtree check_kdtree)

add_executable(check_camera check_camera.cpp)
add_test(check_camera check_camera)

add_executable(check_ransac check_ransac.cpp)
add_test(check_ransac check_ransac)

add_executable(check_eye check_eye.cpp)
add_test(check_eye check_eye)

add_executable(check_radial_k1 check_radial_k1.cpp)
add_test(check_radial_k1 check_radial_k1)

add_executable(check_io_obj check_io_obj.cpp)
add_custom_command(TARGET check_io_obj PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/data
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/data/satosphere.obj ${CMAKE_CURRENT_BINARY_DIR}/data
    )
add_test(check_io_obj check_io_obj)

add_executable(check_instant-meshes check_instant-meshes.cpp)
add_custom_command(TARGET check_instant-meshes PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/data
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/data/semidome.obj ${CMAKE_CURRENT_BINARY_DIR}/data
    )
add_test(check_instant-meshes check_instant-meshes)

add_executable(check_pmp-library check_pmp-library.cpp)
add_custom_command(TARGET check_pmp-library PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/data
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/data/semidome.obj ${CMAKE_CURRENT_BINARY_DIR}/data
    )
add_test(check_pmp-library check_pmp-library)

#
# Code coverage
#
if (TEST_COVERAGE)
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        add_custom_command(OUTPUT test_coverage
            COMMAND mkdir -p ${CMAKE_BINARY_DIR}/coverage
            COMMAND lcov --no-external --capture --initial
                --directory ${CMAKE_SOURCE_DIR}
                --exclude '${CMAKE_SOURCE_DIR}/external/*'
                --exclude '${CMAKE_SOURCE_DIR}/tests/*'
                --output-file coverage/calimiro_base.info
            COMMAND ${CMAKE_MAKE_PROGRAM} test
            COMMAND lcov --no-external --capture
                --directory ${CMAKE_SOURCE_DIR}
                --exclude '${CMAKE_SOURCE_DIR}/external/*'
                --exclude '${CMAKE_SOURCE_DIR}/tests/*'
                --output-file coverage/calimiro.info
            COMMAND lcov --add-tracefile ${CMAKE_BINARY_DIR}/coverage/calimiro_base.info
                --add-tracefile ${CMAKE_BINARY_DIR}/coverage/calimiro.info
                --output-file ${CMAKE_BINARY_DIR}/coverage/calimiro_total.info
            COMMAND genhtml --output-directory ${CMAKE_BINARY_DIR}/coverage ${CMAKE_BINARY_DIR}/coverage/calimiro_total.info
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
            )

        add_custom_target(check_coverage DEPENDS test_coverage)
    endif()
endif()

#
# Integration tests
#
add_executable(check_pipeline_corner check_pipeline_corner.cpp)
add_executable(check_pipeline_fisheye check_pipeline_fisheye.cpp)
add_custom_command(OUTPUT integration_tests
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/data/ ${CMAKE_CURRENT_BINARY_DIR}
    COMMAND ./check_pipeline_corner
    DEPENDS check_pipeline_corner
    COMMAND ./check_pipeline_fisheye
    DEPENDS check_pipeline_fisheye
    )
add_custom_target(check_integration DEPENDS integration_tests)
