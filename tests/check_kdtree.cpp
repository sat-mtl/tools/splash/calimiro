/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // Get asserts in release mode

#include <cassert>
#include <iostream>
#include <vector>

#include <glm/glm.hpp>

#include "./calimiro/kdtree.h"

using namespace calimiro;

int main()
{
    auto kdtree = Kdtree();

    auto points = std::vector<glm::vec3>();
    points.push_back({10.0, 7.0, 3.0});
    points.push_back({5.0, 2.0, 1.0});
    points.push_back({13.0, 4.0, 2.0});
    points.push_back({5.0, 4.0, 1.0});

    // Test the size functionality
    assert((kdtree.size() == 0) == true);
    kdtree.insert(points[0]);
    assert((kdtree.size() == 1) == true);

    // Inserting the remaining points
    kdtree.insert(points[1]);
    kdtree.insert(points[2]);
    kdtree.insert(points[3]);

    // The kdtree now looks like this:
    //
    //
    //                10.0, 7.0, 3.0
    //                  /          \
    //                 /            \
    //                /              \
    //               /                \
    //        5.0, 2.0, 1.0      13.0, 4.0, 2.0
    //              /\                 /\
    //             /  \               /  \
    //            /    \
    //                  \
    //                  5.0, 4.0, 1.0
    //

    // Test the nearest_neighbors_priorities functionality
    std::vector<double> priorities = kdtree.nearest_neighbors_priorities(glm::vec3{5.0, 1.0, 1.0}, 2);
    // The two closest points of the kdtree to (5, 1, 1) are the points: (5, 2, 1) and (5, 4, 1)
    // Corresponding to a priority of 1 and 3 respectively
    std::sort(priorities.begin(), priorities.end());
    assert((priorities[0] == 1) == true);
    assert((priorities[1] == 3) == true);

    // Test the nearest_neighbors functionality
    std::vector<Bqueue<glm::vec3>::Elt> nearest_elts = kdtree.nearest_neighbors(glm::vec3{13.0, 1.0, 2.0}, 1);
    // The closest point to (13, 1, 2) is (13, 4, 2)
    auto elt = nearest_elts[0];
    glm::vec3 closest_pt = *elt.value;
    assert((closest_pt == glm::vec3(13.0, 4.0, 2.0)) == true);

    return 0;
}
